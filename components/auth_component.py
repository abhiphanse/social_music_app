#!/usr/bin/env python

#Python import.
import logging
import datetime
#Local import.
from lib import auth
from lib import util
from lib import constants

#model import.
from model.connection import session
from model import models as model
from model.schema import *

def create_auth_token(user=None, content=None, album=None, expire_after=constants.AUTH_TOKEN_EXPIRE_AFTER, is_expires=True ):
  auth_token = model.OAuthToken()
  auth_token.token = auth.encrypt_password(util.random_number())
  if user:
    auth_token.user_id = user.id
  if content:
    auth_token.content_id = content.id
  if album:
    auth_token.album_id = album.id
  if is_expires:
    auth_token.expires = datetime.datetime.now() + datetime.timedelta(hours=expire_after)
  session.add(auth_token)
  session.flush()
  session.commit()
  return auth_token

def delete_auth_token(auth_token, user, params={} ):
  if auth_token:
    expire_token = session.query(model.OAuthToken).filter(model.OAuthToken.token == auth_token)
    logging.info(expire_token.first())
    expire_token.delete()
    session.commit()
    return True
  else:
    return False

def validate_token(token, user=None, content=None):
    response = {
        "status" : constants.ERROR,
        "data" : None
    }
    status = constants.SUCCESS
    auth_token = session.query(model.OAuthToken).filter(model.OAuthToken.token == token)
    if user:
      auth_token = auth_token.filter(model.OAuthToken.user_id == user.id)
    if content:
      auth_token = auth_token.filter(model.OAuthToken.content_id == content.id)
    auth_token = auth_token.first()
    logging.info(auth_token)
    if not auth_token:
      response["data"] = {"msg":"Invalid link."}
      return response
    elif auth_token.expires and auth_token.expires < datetime.datetime.now():
      response["data"] = {"msg":"Expire link."}
      return response
    response["status"] = constants.SUCCESS
    return response
    
def update_auth_token(auth_token):
  auth_token.token = auth.encrypt_password(util.random_number())
  expire_after=constants.AUTH_TOKEN_EXPIRE_AFTER
  auth_token.expires = datetime.datetime.now() + datetime.timedelta(hours=expire_after)
  session.merge(auth_token)
  session.commit()
  return auth_token
