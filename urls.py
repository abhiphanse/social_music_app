# controllers imports
from controllers import user_controller
from controllers import media_controller
from controllers import task_controller
from controllers import public_controller
from controllers import page_controller
from controllers import api_controller
from controllers import user_api_controller
from controllers import media_api_controller
from controllers import ajax_controller
from controllers import admin_controller
from controllers import party_controller
from controllers import playlist_controller
from controllers import launch_api_controller

url_patterns = [
  # public pages
  ('^/?$', public_controller.IndexPage),
  ('^/artist?$', public_controller.ArtistHomePage),
  ('^/artists?$', public_controller.ArtistsHomePage),
  ('^/index/?$', public_controller.IndexPage),
  #('^/tutorial/?$', public_controller.Tutorial),
  #('^/terms/?$', public_controller.TermsandConditionPage),
  #('^/privacy/?$', public_controller.PrivacyPolicyPage),
  ('^/copyright/?$', public_controller.CopyrightPage),
  ('^/jw_video/?$', public_controller.JwVideo),

  # Cms Domain Pages
  ('^/about/?$', public_controller.AboutCmsHandler),
  ('^/press/?$', public_controller.PressCmsHandler),
  ('^/terms?$', public_controller.TermsCmsHandler),
  ('^/privacy?$', public_controller.PrivacyCmsHandler),
  ('^/help/faq?$', public_controller.FAQCmsHandler),
  ('^/help/getting_started?$', public_controller.HelpCmsHandler),
  ('^/blog?$', public_controller.ContactCmsHandler),

  # launch activities
  ('^/download?$', public_controller.GetAppStoreLink),

  # support urls 
  ('^/delete_contact?$', public_controller.DeleteContact),
  ('^/get_verification_contact?$', public_controller.GetVerificationCode),

  # --- login and home pages ---
  ('^/login/?$', user_controller.Login),
  ('^/logout/?$', user_controller.Logout),
  ('^/facebook_logout?$', user_controller.FacebookLogout),

  # Third-party call
  ('^/fb/post/redirect?$', user_controller.FBPostRedirect),
  ('^/twitter_signin?$', user_controller.SignInWithTwitter),
  ('^/twitter_callback_url?$', user_controller.TwitterCallbackUrl),
  ('^/facebook_callback_url?$', user_controller.FacebookCallbackUrl),
  ('^/twitter_authorised?$', user_controller.TwitterAuthorized),
  ('^/signup/?$', user_controller.SignUp),
  ('^/signup_confirm/?$', user_controller.SignUpConfirm),
  ('^/forgot_username/?$', user_controller.ForgotUserName),
  ('^/forgot_password/?$', user_controller.ForgotPassword),
  ('^/reset_password/?$', user_controller.ResetPassword),
  ('^/remove_account/?$', user_controller.RemoveAccount),
  ('^/verify_account/?$', user_controller.VerifyAccount),
  ('^/invite/?$', user_controller.SendInvitation),
  ('^/settings/?$', user_controller.Settings),

  # Removable Links
  #('^/video/edit/(\d+)?$', media_controller.UploadVideo),
  #('^/video/(?P<content_id>.+)?$', media_controller.SelectVideoFrame),
  #('^/video/?$', media_controller.UploadVideo),
  #('^/album/?$', media_controller.AddAlbum),

  # media controller
  ('^/upload/edit/(\d+)?$', media_controller.UploadContent),
  ('^/upload/?$', media_controller.UploadContent),
  ('^/dashboard?$', media_controller.ListContent),
  ('^/delete_content/?$', media_controller.DeleteContent),
  ('^/album_content/?$', media_controller.AlbumContent),
  ('^/delete_confirmation/?$', media_controller.DeleteConfirmation),
  ('^/cancel_deletion/?$', media_controller.CancelDeletion),
  ('^/undo_deleted_content/?$', media_controller.UndoDeletedContent),
  ('^/undo_album_content/?$', media_controller.UndoDeletedAlbum),
  ('^/set_content_duration/?$', media_controller.SetContentDuration),
  ('^/set_user_image/?$', user_controller.SetUserImage),  
    
  # page controller
  ('^/pages/?$', page_controller.EditInfoPage),
  ('^/discography/?$', page_controller.ArtistDiscographyPage),
  ('^/images/?$', page_controller.ShowImagePage),
  ('^/images/profile/(\d+)?$', page_controller.ShowProfilePicture),  
  ('^/images/(\d+)?$', page_controller.ShowArtistImage),  
  #('^/undo_content/?$', media_controller.UndoContent),

  # Crone urls
  ('^/send_emails_to_mass_users?$', admin_controller.SendEmailsToMassUsers),
  ('^/send_emails_to_cmj_artists?$', admin_controller.SendEmailsToCmjArtists),
  # Task queue urls
  ('^/tasks/reset_username_email?$', task_controller.ResetUserNameEmail),
  ('^/tasks/reset_password_email?$', task_controller.ResetPasswordEmail),  
  ('^/tasks/send_welcome_email?$', task_controller.SendWelcomeEmail),
  ('^/tasks/resend_verification_email?$', task_controller.ResendVerificationEmail),

  ('^/tasks/send_invitation_mail?$', task_controller.SendInvitationMail),
  ('^/tasks/delete_expire_contents?$', task_controller.DeleteExpireContents),
  ('^/tasks/delete_confirmation?$', task_controller.DeleteConfirmation),
  ('^/tasks/send_admin_delete_mail?$', task_controller.SendAdminDeleteMail),
  ('^/tasks/delete_confirm_email?$', task_controller.DeleteConfirmMail),
  ('^/tasks/email_support_log?$', task_controller.EmailSupportLog),
  ('^/tasks/create_current_streaming?$', task_controller.CreateCurrentStreaming),
  ('^/tasks/stop_current_streaming?$', task_controller.StopCurrentStreaming),
  ('^/tasks/update_hourly_content_stats?$', task_controller.UpdateHourlyContentStats),
  ('^/tasks/delete_expire_albums?$', task_controller.DeleteExpireAlbums),
  ('^/tasks/add_hourly_stat?$', task_controller.AddHourlyStats),
  ('^/tasks/get_content_duration?$', task_controller.GetContentDurationAndCoverFrame),
  ('^/tasks/convert_media_codec/?$', task_controller.ConvertMediaCodec),
  ('^/tasks/create_cover_frames?$', task_controller.CreateCoverFrames),
  ('^/tasks/save_default_cover_frames?$', task_controller.SaveDefaultCoverFrames),

  ('^/tasks/support_email_for_content_processing_error?$', task_controller.SupportEmailForContentProcessingError),

  ('^/tasks/slash_weekly_vote_count?$', task_controller.SlashWeeklyVoteCount),
  ('^/tasks/update_social_friends?$', task_controller.UpdateSocialFriends),
  ('^/tasks/update_facebook_note_friends?$', task_controller.UpdateFacebookNoteFriends),
  ('^/tasks/update_twitter_note_friends?$', task_controller.UpdateTwitterNoteFriends),

  ('^/tasks/blur_album_image?$', task_controller.BlurAlbumImage),
  ('^/tasks/blur_existing_album_image?$', task_controller.BlurExistingAlbumImage),
  ('^/tasks/blur_user_image?$', task_controller.BlurUserImage),  
  ('^/tasks/blur_existing_user_image?$', task_controller.BlurExistingUserImage),

  ('^/tasks/send_notifiation_to_device?$', task_controller.SendNotifiationToDevice),
  ('^/tasks/remove_expire_sessions?$', task_controller.RemoveExpireSessions),
  ('^/tasks/delete_search_index?$', task_controller.DeleteSearchIndex),
  ('^/tasks/notification_for_conversation?$', task_controller.ConversationNotification),
  ('^/tasks/notification_for_joined_user?$', task_controller.UserJoinNotification),
  ('^/tasks/notification_for_social_friends_synced?$', task_controller.SocialFriendsSyncedNotification),
  ('^/tasks/logrelay/?$', task_controller.LogRelay),

  # launch stretegy. 
  ('^/tasks/send_mobile_sms?$', task_controller.SendMobileSMS),
  ('^/tasks/delete_expire_reserve_spots?$', task_controller.DeleteExpireReserveSpots),
  ('^/tasks/send_reminder_email_for_signup?$', task_controller.SendReminderEmailForSignup),
  ('^/tasks/remind_to_reserve_spots_for_signup?$', task_controller.RemindToReserveSpotsForSignup),
  ('^/tasks/reserve_spot_succeed_email?$', task_controller.ReserveSpotSucceedEmail),

  ('^/tasks/notification_for_unfollow?$', task_controller.UnFollowNotification),
  ('^/tasks/notification_for_change_profile_image?$', task_controller.ChangeProfileImageNotification),

  ('^/tasks/upload_on_s3?$', task_controller.UploadImageOnS3),
  ('^/tasks/upload_playlist_image_on_s3?$', task_controller.UploadPlaylistImageOnS3),
  ('^/tasks/update_library_contents?$', task_controller.UpdateLibraryContent),
  ('^/tasks/update_library_content_to_set_hidden?$', task_controller.UpdateLibraryContenttoSetHidden),
  ('^/tasks/update_library_album_to_set_hidden?$', task_controller.UpdateLibraryAlbumtoSetHidden),

  ('^/tasks/update_ds_entity?$', task_controller.UpdateDSEntity),
  ('^/tasks/delete_ds_entity?$', task_controller.DeleteDSEntity),
  ('^/tasks/update_ds_content_activity?$', task_controller.UpdateDSContentActivity),

  ('^/tasks/notification_for_user_profile_update?$', task_controller.UserProfileUpdateNotification),
  ('^/tasks/unread_post_activites?$', task_controller.UnreadPostActivites),
  ('^/tasks/read_conversation?$', task_controller.ReadConversation),
  ('^/tasks/update_follow_detail?$', task_controller.UpdateFollowDetail),
  ('^/tasks/send_confirmation_email_to_move_beta_users?$', task_controller.SendConfirmationEmailToMoveBetaUsers),
  ('^/tasks/notification_for_beta_user_moved_into_app?$', task_controller.NotificationForBetaUserMovedIntoApp),
  ('^/tasks/update_already_signed_up_beta_users?$', task_controller.UpdatelreadySignedUpBetaUsers),

  ('^/tasks/send_confirmation_email_to_move_into_app?$', task_controller.SendConfirmationEmailToMoveIntoApp),
  ('^/tasks/notification_for_cluster_moved_into_app?$', task_controller.NotificationForClusterMovedIntoApp),
  ('^/tasks/update_user_contacts_for_resync_contact?$', task_controller.UpdateUserContactForResynContact),
  ('^/tasks/update_contact_index?$', task_controller.UpdateContactIndex),
  ('^/tasks/send_emails_to_mass_users?$', task_controller.SendEmailsToMassUsers),
  ('^/tasks/send_limited_emails_to_mass_users?$', task_controller.SendLimitedEmailsToMassUsers),
  ('^/tasks/send_emails_to_beta_released_emails?$', task_controller.SendEmailsToBetaReleasedEmails),
  ('^/tasks/send_limited_emails_to_beta_released_emails?$', task_controller.SendLimitedEmailsToBetaReleasedEmails),
  ('^/tasks/add_emails_for_beta_releated_emails?$', task_controller.AddEmailsForBetaReleatedEmails),


  ('^/tasks/send_emails_to_cmj_artists?$', task_controller.SendEmailsToCmjArtists),
  ('^/tasks/delete_s3_data?$', task_controller.DeleteS3Data),
  ('^/tasks/update_party_playlist?$', task_controller.UpdatePartyPlaylist),

  ('^/tasks/delete_real_time_content_activity?$', task_controller.DeleteRealTimeContentActivity),
  ('^/tasks/delete_past_hour_content_activity?$', task_controller.DeletePastHourContentActivity),
  ('^/tasks/upload_user_compressed_image?$', task_controller.UploadUserCompressedImage),
  ('^/tasks/upload_album_compressed_image?$', task_controller.UploadAlbumCompressedImage),
  ('^/tasks/hard_delete_user?$', task_controller.HardDeleteUser),

  # api urls
  ('^/api/login?$', api_controller.Login),
  ('^/api/logout?$', api_controller.Logout),
  ('^/api/signup?$', api_controller.SignUp),
  ('^/api/editsettings?$', api_controller.EditSettings),
  ('^/api/getsettings?$', api_controller.GetSettings),
  ('^/api/check_user_availability?$', api_controller.CheckUserAvailability),
  ('^/api/forgot_username?$', api_controller.ForgotUsername),
  ('^/api/forgot_password?$', api_controller.ForgotPassword),

  # user api urls
  ('^/api/user/(\d+)$', user_api_controller.Settings),
  ('^/api/user/(\d+)/real_time_feeds?$', user_api_controller.RealTimeFeeds),
  ('^/api/user/(\d+)/real_time_feeds_count?$', user_api_controller.RealTimeFeedsCount),  
  ('^/api/register_follower?$', user_api_controller.RegisterFollower),
  ('^/api/un_register_follower?$', user_api_controller.UnRegisterFollower),
  ('^/api/get_following_list?$', user_api_controller.GetFollowingList),
  ('^/api/get_follower_list?$', user_api_controller.GetFollowerList),
  ('^/api/search?$', user_api_controller.FullTextSearch),
  ('^/api/reports?$', user_api_controller.AdminReportAPI),
  ('^/api/supportlog?$', user_api_controller.SaveSupportLog),
  ('^/api/user/(\d+)/blur_image?$', user_api_controller.UploadUserBlurImage),
  ('^/api/user/(\d+)/friends?$', user_api_controller.GetUserFriendsList),

  # media activities
  ('^/api/get_album_list?$', media_api_controller.GetAlbums),
  ('^/api/content/(\d+)$', media_api_controller.GetContent),
  ('^/api/content/(\d+)/stream$', media_api_controller.GetContentStream),
  ('^/api/content/(\d+)/add_to_playlists$', media_api_controller.AddToPlaylists),
  ('^/api/content/(\d+)/convert_mp3_to_codec?$', media_api_controller.ConvertMp3ToCodec),
  ('^/api/content/(\d+)/save_video_frames?$', media_api_controller.SaveVideoFrames),
  ('^/api/contents?$', media_api_controller.GetContents),
  ('^/api/genres?$', media_api_controller.GetGenres),
  ('^/api/comment/(\d+)$', media_api_controller.GetComment),
  ('^/api/search?$', user_api_controller.FullTextSearch),
  ('^/api/specific-search$', user_api_controller.SpecificTextSearch),
  ('^/api/library-search$', user_api_controller.LibraryTextSearch),
  ('^/api/user-search$', user_api_controller.UserSearch),
  ('^/api/friend-search$', user_api_controller.FriendSearch),
  ('^/api/gallery?$', user_api_controller.Gallery),
  ('^/api/artist-count?$', user_api_controller.GetArtistCount),
  ('^/api/juke_box?$', media_api_controller.JukeBox),
  ('^/api/users?$', media_api_controller.GetUsers),
  ('^/api/parties?$', media_api_controller.GetParties),
  ('^/api/album/(\d+)?$', media_api_controller.GetAlbum),

  ('^/api/user/(\d+)/updates?$', media_api_controller.PostActivities),
  ('^/api/user/(\d+)/library?$', media_api_controller.GetLibrary),
  ('^/api/user/(\d+)/contents?$', media_api_controller.GetUserContents),
  ('^/api/post/(\d+)/comments?$', media_api_controller.PostComments),
  ('^/api/post/(\d+)/likes?$', media_api_controller.PostLikes),
  ('^/api/post/(\d+)?$', media_api_controller.PostDetail),
  ('^/api/post_comment/(\d+)?$', media_api_controller.PostComment),
  ('^/api/user/(\d+)/party?$', party_controller.CreateParty),
  ('^/api/user/(\d+)/notification_toggles?$', user_api_controller.UserNotificationToggles),
  ('^/api/user/(\d+)/user_metadata?$', user_api_controller.UserMetaData),
  ('^/api/user/(\d+)/update_device_info?$', user_api_controller.UserDeviceInfo),
  ('^/api/user/(\d+)/update_image?$', user_api_controller.UpdateUserImage),

  ('^/api/album/(\d+)/blur_image?$', media_api_controller.UploadBlurImage),
  ('^/api/artist_page/(\d+)?$', user_api_controller.ArtistPage),
  ('^/api/page/(\d+)?$', user_api_controller.UserArtistPage),

  # playlist activities
  ('^/api/playlist/(\d+)?$', playlist_controller.Playlist),
  ('^/api/playlist/(\d+)/remove_share?$', playlist_controller.RemovePlaylistShare),

  # party activities
  ('^/api/party/(\d+)?$', party_controller.PartySetting),    
  ('^/api/user/(\d+)/timeline?$', party_controller.Timeline),
  ('^/api/party/(\d+)/partyend?$', party_controller.PartyEnd),

  # launch activities
  ('^/api/user/(\d+)/verification?$', launch_api_controller.MobileVerification),
  ('^/api/user/(\d+)/email_verification?$', launch_api_controller.EmailVerification),
  ('^/api/user/(\d+)/reservation?$', launch_api_controller.UserReservation),
  ('^/api/user/(\d+)/contacts?$', launch_api_controller.MobileContacts),
  ('^/api/user/(\d+)/sms-invitation?$', launch_api_controller.SmsInvitation),
  ('^/api/user/(\d+)/social-friends?$', launch_api_controller.GetSocialFriends),
  ('^/api/user/(\d+)/cluster?$', launch_api_controller.Cluster),
  ('^/api/user/(\d+)/social_network_sync?$', launch_api_controller.SocialNetworkSync),
  ('^/api/user/(\d+)/unsync_facebook?$', launch_api_controller.UnSyncFacebookAccount),
  ('^/api/user/(\d+)/unsync_twitter?$', launch_api_controller.UnSyncTwitterAccount),

  ('^/api/user/(\d+)/conversation?$', user_api_controller.UserConversation),
  ('^/api/user/(\d+)/conversation_count?$', user_api_controller.UserConversationCount),

  ('^/api/social-invitation?$', user_api_controller.SocialInvitation),

  #ajax urls
  ('^/ajax/album_sortedid_change?$', ajax_controller.AlbumSortedIdChange),
  ('^/ajax/content_sortedid_change?$', ajax_controller.ContentSortedIdChange),
  ('^/ajax/pic_sortedid_change?$', ajax_controller.PicSortedIdChange),
  ('^/ajax/check_user_availability?$', ajax_controller.CheckUserAvailability),
  ('^/ajax/check_page_availability?$', ajax_controller.CheckPageAvailability),
  ('^/ajax/check_album_availability?$', ajax_controller.CheckAlbumAvailability),  
  ('^/ajax/delete_account?$', ajax_controller.DeleteAccount),
  ('^/ajax/get_edit_artist_data?$', ajax_controller.GetEditArtistData),
  ('^/ajax/delete_artist_image?$', ajax_controller.DeleteArtistImage),
  ('^/ajax/upload_image?$', ajax_controller.UploadImages),
  ('^/ajax/change_album_image?$', ajax_controller.ChangeAlbumImage),
  ('^/ajax/delete_report?$', ajax_controller.DeleteReport),
  ('^/ajax/delete_artist_page?$', ajax_controller.DeleteArtistPage),
  ('^/ajax/resend_email?$', ajax_controller.ResendEmail),
  ('^/ajax/login?$', ajax_controller.Login),
  ('^/ajax/signup?$', ajax_controller.Signup),
  ('^/ajax/logout?$', ajax_controller.Logout),
  ('^/ajax/create_album?$', ajax_controller.CreateAlbum),


  ('^/ajax/forgot_password?$', ajax_controller.ForgotPassword),
  ('^/ajax/update_follow_detail?$', ajax_controller.UpdateFollowDetail),

  ('^/ajax/get_app_store_link?$', ajax_controller.SendDownloadAppLink),
  ('^/ajax/reserve_spot?$', ajax_controller.ReserveSpot),
  ('^/ajax/save-invite-email?$', ajax_controller.SaveInviteEmail),
  ('^/ajax/save-promotional-emails?$', ajax_controller.SavePromotionalEmails),

  ('^/ajax/change_profile_image?$', ajax_controller.ChangeProfileImage),
  ('^/ajax/give_access_to_beta_user?$', ajax_controller.GiveAccessToBetaUser),
  ('^/ajax/give_access_to_cluster_users?$', ajax_controller.GiveAccessToClusterUser),
  ('^/ajax/send_reservation_number_to_artist?$', ajax_controller.SendReservationNumberToArtist),
  ('^/ajax/content/(\d+)?$', ajax_controller.GetContentDetail),
  ('^/ajax/album/(\d+)/update_album?$', ajax_controller.UpdateAlbumDetail),
  ('^/ajax/content/(\d+)/update?$', ajax_controller.UpdateContent),
  ('^/ajax/user/(\d+)/unsync?$', ajax_controller.UnsyncAccount),
  ('^/ajax/upload_content?$', ajax_controller.UploadContent),
  ('^/ajax/delete_content?$', ajax_controller.DeleteContent),
  ('^/ajax/upload_artist_image?$', ajax_controller.UploadArtistImage),
  ('^/ajax/upload_artist_page?$', ajax_controller.UploadArtistPage),
  ('^/ajax/add-beta-users?$', ajax_controller.AddBetaUsers),

  #admin urls
  ('^/admin/song_report?$', admin_controller.SongReport),
  ('^/admin/post_report?$', admin_controller.PostReport),
  ('^/admin/user_report?$', admin_controller.UserReport),
  ('^/hiddenforstaff?$', admin_controller.GetCount),
  ('^/hiddenforstaff/user_statistics?$', admin_controller.UserSignedUpStatistic),

  ('^/admin/artists?$', admin_controller.ArtistDetail),
  ('^/admin/clusters?$', admin_controller.ClusterDetail),
  ('^/admin/emails?$', admin_controller.SaveEmails),
  ('^/admin/beta_users?$', admin_controller.GetBetaInvitees),
  ('^/admin/add_beta_user?$', admin_controller.AddBetaUser),
  ('^/admin/unsuspend_user?$', admin_controller.UnsuspendUser),
  ('^/admin/action_for_post_report?$', admin_controller.ActionForPostReport),
  ('^/admin/delete_account?$', admin_controller.DeleteAccountAdmin),
  ('/.*', public_controller.NotFoundPageHandler),
]

