#!/usr/bin/env python
from sqlalchemy import orm
import datetime
from sqlalchemy import schema, types
from sqlalchemy.dialects.mysql import TINYINT

metadata = schema.MetaData()

def now():
    return datetime.datetime.now()

# user_profile table
user_table = schema.Table('users', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('user_seq_id', optional=True), primary_key=True),
    schema.Column('name', types.String(50)),
    schema.Column('username', types.String(50)),
    schema.Column('email', types.String(62), nullable=False), # TODO change its type to email
    schema.Column('password', types.String(64), nullable=False), # TODO change its type to email
    schema.Column('fb_name', types.String(50)),
    schema.Column('twitter_name', types.String(50)),
    schema.Column('is_active', TINYINT(1), default=True),
    schema.Column('is_first_time', types.Boolean(), default=False),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
    schema.Column('last_login', types.TIMESTAMP()),
    schema.Column('twitter_id', types.String(50) ),
    schema.Column('fb_id', types.String(50) ),
    schema.Column('country_code', types.String(10)),
    schema.Column('fb_profile_picture_url', types.String(100)),
    
    schema.Column('user_type', TINYINT(1), nullable=True), # TODO change its type to bool
    schema.Column('is_admin', types.Boolean(), default=False),

    schema.Column('facebook_token', types.String(500), nullable=True),
    schema.Column('twitter_token', types.String(500), nullable=True),
    schema.Column('twitter_secret', types.String(500), nullable=True),

    schema.Column('image_id', types.Integer, schema.ForeignKey('images.id', ondelete="CASCADE")),
    schema.Column('blur_image_id', types.Integer, schema.ForeignKey('images.id', ondelete="CASCADE")),
    schema.Column('fb_toggle', types.Boolean(), default=True),
    schema.Column('twitter_toggle', types.Boolean(), default=True),
    schema.Column('playlist_toggle', types.Boolean(), default=False),
    schema.Column('contact_number', types.String(50)),
    schema.Column('full_contact_number', types.String(50)),
    schema.Column('is_sync_contacts', types.Boolean(), default=False),
    schema.Column('is_launch_complete', types.Boolean(), default=False),
    schema.Column('s3_image_key', types.String(255), nullable=True),
    schema.Column('s3_compressed_image_key', types.String(255), nullable=True),
    schema.Column('s3_rescale_image_key', types.String(255), nullable=True),
    schema.Column('s3_blur_image_key', types.String(255), nullable=True),
    schema.Column('following_count', types.Integer, default=0),
    schema.Column('follower_count', types.Integer, default=0),
    schema.Column('is_email_verified', types.Boolean(), default=False),
    schema.Column('is_fb_friends_synced', types.Boolean(), default=False),
    schema.Column('is_twitter_friends_synced', types.Boolean(), default=False),
    schema.Column('cluster_id', types.Integer),
    schema.Column('facebook_user_permission', TINYINT(2), nullable=True),
)

# Genre table
genre_table = schema.Table('genres', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('genre_seq_id', optional=True), primary_key=True),
    schema.Column('name', types.String(50)),
)

# Image table
image_table = schema.Table('images', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('image_seq_id', optional=True), primary_key=True),
    schema.Column('name', types.String(45)),
    schema.Column('caption', types.Text()),
    schema.Column('sort_order', types.Integer),
    schema.Column('full_file_loc', types.String(255)),
    schema.Column('thumb_file_loc', types.String(255)),

    schema.Column('s3_key', types.String(255)),
    schema.Column('image_height', types.Integer),
    schema.Column('image_weight', types.Integer),
    schema.Column('s3_thumb_image_key', types.String(255)) 

)

# Album table
album_table = schema.Table('albums', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('album_seq_id', optional=True), primary_key=True),
    schema.Column('album_name', types.String(45)),

    schema.Column('info', types.Text()),
    schema.Column('image_id', types.Integer, schema.ForeignKey('images.id', ondelete="CASCADE")),
    schema.Column('blur_image_id', types.Integer, schema.ForeignKey('images.id', ondelete="CASCADE")),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),

    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
    schema.Column('sort_order', types.Integer),
    schema.Column('is_hidden', TINYINT(2), default=0),
    schema.Column('expire_time', types.TIMESTAMP()),

    schema.Column('s3_image_key', types.String(255), nullable=False),
    schema.Column('s3_compressed_image_key', types.String(255), nullable=True),
    schema.Column('s3_blur_image_key', types.String(255), nullable=True),
    schema.Column('s3_fb_post_image_key', types.String(255), nullable=True),
)

# Content table
content_table = schema.Table('contents', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('content_seq_id', optional=True), primary_key=True),
    schema.Column('lyrics', types.Text()),
    schema.Column('title', types.String(100), default=u'Untitled Page'),
    schema.Column('heading', types.String(100)),
    schema.Column('duration', types.Integer(20), default=0), #TODO change its type to time
    schema.Column('format', types.String(50)),
    schema.Column('file_url', types.String(255)),  #TODO change its type to url
    schema.Column('file_name', types.String(255)),
    schema.Column('content_type', TINYINT(2)), # content type 1 for Video and type 2 for Audio type
    schema.Column('buckets', types.String(50)),
    schema.Column('is_hidden', TINYINT(2), default=0),
    schema.Column('sort_order', types.Integer),

    schema.Column('expire_time', types.TIMESTAMP()),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),

    schema.Column('s3_response', types.Text()),
    schema.Column('s3_etag', types.String(255)),
    schema.Column('s3_location', types.String(255)), #TODO change its type to url
    schema.Column('s3_Key', types.String(255)),
    schema.Column('s3_Key_before_convert', types.String(255)),

    schema.Column('s3_frame_url', types.String(255)), 
    schema.Column('s3_frame_Key', types.String(255)),
    schema.Column('video_frame', types.String(255)),

    schema.Column('genre_id', types.Integer, schema.ForeignKey('genres.id', ondelete="CASCADE")),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('album_id', types.Integer, schema.ForeignKey('albums.id', ondelete="CASCADE")),

    schema.Column('current_playing_score', types.DECIMAL(10,4), default=0.0),
    schema.Column('play_count', types.Integer, default=0),
    schema.Column('share_count', types.Integer, default=0),
    schema.Column('comment_count', types.Integer, default=0),
    schema.Column('like_count', types.Integer, default=0),
    schema.Column('real_time_score', types.DECIMAL(10,4), default=0.0),
    schema.Column('is_converted', types.Boolean(), default=False),
    schema.Column('processing_status', TINYINT(2), default=0),
    schema.Column('video_frame_width', types.Integer, default=0),
    schema.Column('video_frame_height', types.Integer, default=0),
    schema.Column('processing_attempted', types.Boolean(), default=False),
)

# Comments table
comment_table = schema.Table('comments', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('comment_seq_id', optional=True), primary_key=True),
    schema.Column('comment', types.Text()),
    schema.Column('vote', types.Integer, default=0),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),

)

# Shares table
share_table = schema.Table('shares', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('share_seq_id', optional=True), primary_key=True),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('playlist_id', types.Integer, schema.ForeignKey('playlists.id', ondelete="CASCADE")),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('to_user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('share_type', types.String(20)), # share_type can be content, playlist
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('share_comment', types.Text()),
)

# Like table
like_table = schema.Table('likes', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('like_seq_id', optional=True), primary_key=True),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
)

# Auth Token Table
auth_token_table = schema.Table('oauth_tokens', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('authtoken_seq_id', optional=True), primary_key=True),
    schema.Column('token', types.String(255)),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('album_id', types.Integer, schema.ForeignKey('albums.id', ondelete="CASCADE")),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('expires', types.TIMESTAMP()),
)

# Followers
followers_table = schema.Table('followers', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('followers_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('follower_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('share_count', types.Integer, default=0),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
)

# Fan List
fan_list_table = schema.Table('fanlist', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('fanlist_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('email', types.String(255)),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
)

# Artist Data
artist_data_table = schema.Table('artistdata', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('artistdata_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('page_name', types.String(45)),
    schema.Column('page_info', types.BLOB()),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)

# Artist Image
artist_image_table = schema.Table('artistimages', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('artistimages_seq_id', optional=True), primary_key=True),
    schema.Column('artist_data_id', types.Integer, schema.ForeignKey('artistdata.id', ondelete="CASCADE")),
    schema.Column('image_id', types.Integer, schema.ForeignKey('images.id', ondelete="CASCADE")),
)

# Playlist
playlist_table = schema.Table('playlists', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('playlists_seq_id', optional=True), primary_key=True),
    schema.Column('name', types.String(100)),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('party_id', types.Integer, schema.ForeignKey('parties.id', ondelete="CASCADE")),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('is_public', types.Boolean(), default=False),
    schema.Column('image_id', types.Integer, schema.ForeignKey('images.id', ondelete="CASCADE")),

    schema.Column('s3_image_key', types.String(255), nullable=True),
)

# Party
party_table = schema.Table('parties', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('parties_seq_id', optional=True), primary_key=True),
    schema.Column('name', types.String(100)),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('passcode', types.String(45)),
    schema.Column('is_public', types.Boolean(), default=False),

    schema.Column('is_unlisted', types.Boolean(), default=False),
    schema.Column('is_passcode_enabled', types.Boolean(), default=False),
    schema.Column('host_queue_id', types.Integer),
    schema.Column('guest_queue_id', types.Integer),
    schema.Column('playlist_queue_id', types.Integer),
    schema.Column('post_in_feeds', types.Boolean(), default=False),
    schema.Column('is_deleted', types.Boolean(), default=False),
    schema.Column('expiration_time', types.TIMESTAMP()),
    schema.Column('is_playlist_save', types.Boolean(), default=False),

    schema.Column('added_date', types.TIMESTAMP(), default=now()),
)

party_playlist_table = schema.Table('party_playlists', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('party_playlists_seq_id', optional=True), primary_key=True),
    schema.Column('party_id', types.Integer, schema.ForeignKey('parties.id', ondelete="CASCADE")),
    schema.Column('playlist_id', types.Integer, schema.ForeignKey('playlists.id', ondelete="CASCADE")),

    schema.Column('added_date', types.TIMESTAMP(), default=now()),
)

party_content_queue_table = schema.Table('party_content_queue', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('content_queues_seq_id', optional=True), primary_key=True),
    schema.Column('queue_id', types.Integer ),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('is_playing', types.Boolean(), default=False ),
    schema.Column('played_time', types.TIMESTAMP()),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
)

party_guest_table = schema.Table('party_guests', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('party_guests_seq_id', optional=True), primary_key=True),
    schema.Column('party_id', types.Integer, schema.ForeignKey('parties.id', ondelete="CASCADE")),
    schema.Column('guest_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('is_deleted', types.Boolean(), default=False),
    schema.Column('role', types.String(10)),

    schema.Column('is_playlist_save', types.Boolean(), default=False),
    schema.Column('playlist_id', types.Integer, schema.ForeignKey('playlists.id')),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
)

party_queue_table = schema.Table('party_queue', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('party_queue_seq_id', optional=True), primary_key=True),
    schema.Column('party_id', types.Integer, schema.ForeignKey('parties.id', ondelete="CASCADE")),
    schema.Column('queue_type', types.String(20) ),

    schema.Column('added_date', types.TIMESTAMP(), default=now()),
)

content_played_history_table = schema.Table('content_played_history', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('content_played_history_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('start_time', types.TIMESTAMP()),
    schema.Column('end_time', types.TIMESTAMP()),
    schema.Column('is_playing', types.Boolean(), default=True),
    schema.Column('party_id', types.Integer()),
    schema.Column('stream_by', types.String(10)),
)

hourly_content_stats_table = schema.Table('hourly_content_stats', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('hourly_content_stats_seq_id', optional=True), primary_key=True),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('like_count', types.Integer, default=0),
    schema.Column('comment_count', types.Integer, default=0),
    schema.Column('share_count', types.Integer, default=0),
    schema.Column('play_count', types.Integer, default=0),
    schema.Column('total_score', types.DECIMAL(10,4), default=0.0),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
)

report_table = schema.Table('reports', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('reports_seq_id', optional=True), primary_key=True),
    schema.Column('reporter_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('reportee_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('post_id', types.Integer, schema.ForeignKey('posts.id', ondelete="CASCADE")),
    schema.Column('reason', types.Text()),
    schema.Column('caption', types.Text()),
    schema.Column('report_type', types.Unicode(10)),
    schema.Column('report_date', types.TIMESTAMP(), default=now()),
)
 
playlist_content_table = schema.Table('playlist_contents', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('playlist_contents_seq_id', optional=True), primary_key=True),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('playlist_id', types.Integer, schema.ForeignKey('playlists.id', ondelete="CASCADE")),

    schema.Column('added_date', types.TIMESTAMP(), default=now()),
)

post_table = schema.Table('posts', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('post_seq_id', optional=True), primary_key=True),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('party_id', types.Integer, schema.ForeignKey('parties.id', ondelete="CASCADE")),
    schema.Column('playlist_id', types.Integer, schema.ForeignKey('playlists.id', ondelete="CASCADE")),
    schema.Column('shared_user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('gallery_vote_id', types.Integer, schema.ForeignKey('gallery_album_votes.id', ondelete="CASCADE")),
    schema.Column('event_type', types.String(20)), # event can be upload, share, create
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), default=now()),
    schema.Column('event_for', types.String(20)), # event can be content=1, playlist =2
    schema.Column('comment', types.Text()),
    schema.Column('comment_id', types.Integer, schema.ForeignKey('comments.id', ondelete="CASCADE")),
)

post_activity_table = schema.Table('post_activities', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('post_activity_seq_id', optional=True), primary_key=True),
    schema.Column('post_id', types.Integer, schema.ForeignKey('posts.id', ondelete="CASCADE")),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('post_user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('follower_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('comment', types.Text()),
    schema.Column('activity_type', types.String(20)), # activity type can be like, comment, repost, report
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('is_viewed', types.Boolean(), default=False),
)

user_comment_vote_table = schema.Table('user_comment_votes', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('user_comment_votes_seq_id', optional=True), primary_key=True),
    schema.Column('comment_id', types.Integer, schema.ForeignKey('comments.id', ondelete="CASCADE")),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('vote_type', TINYINT(1)),
    schema.Column('vote_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)

gallery_album_vote_table = schema.Table('gallery_album_votes', metadata,
    schema.Column('id', types.Integer,
        schema.Sequence('gallery_album_votes_seq_id', optional=True), primary_key=True),
    schema.Column('album_id', types.Integer, schema.ForeignKey('albums.id', ondelete="CASCADE")),
    schema.Column('gallery_album_id', types.Integer, schema.ForeignKey('gallery_albums.id', ondelete="CASCADE")),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('vote_type', TINYINT(1)),
    schema.Column('vote_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)

gallery_albums_table = schema.Table('gallery_albums', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('gallery_albums_seq_id', optional=True), primary_key=True),
    schema.Column('album_id', types.Integer, schema.ForeignKey('albums.id', ondelete="CASCADE")),
    schema.Column('vote_count', types.DECIMAL(10,4), default=0.0),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)

social_network_table = schema.Table('social_networks', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('social_network_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('name', types.String(20)),
    schema.Column('profile_picture_url', types.String(100)),
    schema.Column('network_id', types.String(255)),
    schema.Column('facebook_public_id', types.String(255)),
    schema.Column('network_type', types.String(20)),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
    schema.Column('is_invited', types.Boolean(), default=False),
    schema.Column('friend_our app_user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
)

# Eight Image table
profile_image_table = schema.Table('profile_images', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('profile_images_seq_id', optional=True), primary_key=True),
    schema.Column('full_file_loc', types.String(255)),
    schema.Column('sort_order', types.Integer),
)

# Social Network sharing table 
social_sharing_table = schema.Table('social_sharing', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('social_sharing_seq_id', optional=True), primary_key=True),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('network_id', types.String(50)),
    schema.Column('network_type', types.String(20)),
    schema.Column('share_comment', types.Text()),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),       
)

# Mobile Support Log table 
support_issue_table = schema.Table('support_issues', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('support_issues_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('title', types.String(90)),
    schema.Column('description', types.Text()),
    schema.Column('logfile_key', types.String(255)),
    schema.Column('logfile_name', types.String(255)),
    schema.Column('s3_image_key', types.String(255)),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),       
)

user_thread_table = schema.Table('user_threads', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('user_thread_seq_id', optional=True), primary_key=True),
    schema.Column('user_1_id', types.Integer, schema.ForeignKey('users.id')),
    schema.Column('user_2_id', types.Integer, schema.ForeignKey('users.id')),
    schema.Column('user_1_new_message', types.Boolean()),
    schema.Column('user_2_new_message', types.Boolean()),
    schema.Column('added_datetime', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP() ),
    schema.Column('user_1_modified_datetime', types.TIMESTAMP()),
    schema.Column('user_2_modified_datetime', types.TIMESTAMP()),
)

conversation_thread_table = schema.Table('conversation_threads', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('conversation_thread_seq_id', optional=True), primary_key=True),
    schema.Column('thread_id', types.Integer, schema.ForeignKey('user_threads.id')), #TODO Need to add cluster for thread id
    schema.Column('sender_id', types.Integer, schema.ForeignKey('users.id')),
    schema.Column('receiver_id', types.Integer, schema.ForeignKey('users.id')),
    schema.Column('post_activity_id', types.Integer, schema.ForeignKey('post_activities.id')),
    schema.Column('text', types.Text()),
    schema.Column('sent_datetime', types.TIMESTAMP(), default=now()),
    schema.Column('read_datetime', types.TIMESTAMP(), onupdate=now()),       
)

user_notification_toggles_table = schema.Table('user_notification_toggles', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('user_notification_toggles_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id')), #TODO Need to add cluster for thread id
    schema.Column('follower_toggle', types.Boolean()),
    schema.Column('like_toggle', types.Boolean()),
    schema.Column('comment_toggle', types.Boolean()),
    schema.Column('contact_join_toggle', types.Boolean()),
    schema.Column('facebook_join_toggle', types.Boolean()),
    schema.Column('twitter_join_toggle', types.Boolean()),
    schema.Column('private_message_toggle', types.Boolean()),
    schema.Column('unsubscribe_email_toggle', types.Boolean()),
    schema.Column('added_datetime', types.TIMESTAMP(), default=now()),
    schema.Column('modify_datetime', types.TIMESTAMP(), onupdate=now()),
)

library_contents_table = schema.Table('library_contents', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('library_contents_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('playlist_id', types.Integer, schema.ForeignKey('playlists.id', ondelete="CASCADE")),
    schema.Column('added_datetime', types.TIMESTAMP(), default=now()),
    schema.Column('modify_datetime', types.TIMESTAMP(), onupdate=now()),
)

# Sms Verification Codes table.
verification_codes_table = schema.Table('verification_codes', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('verification_codes_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('verification_code', types.String(30)),
    schema.Column('contact_number', types.String(50)),
    schema.Column('country_code', types.String(10)),
    schema.Column('expire_time', types.TIMESTAMP()),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)

# reservation number.
reservation_numbers_table = schema.Table('reservation_numbers', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('reservation_numbers_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('reservation_number', types.String(30)),
    schema.Column('contact_number', types.String(50)),
    schema.Column('expire_time', types.TIMESTAMP()),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)

# Cluster Table.
clusters_table = schema.Table('clusters', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('clusters_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('user_count', types.Integer, default=0),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('is_moved_in', types.Boolean(), default=False),
    schema.Column('total_score', types.Integer, default=0),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),     
)

# Cluster Member Table.
cluster_users_table = schema.Table('cluster_users', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('cluster_users_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('cluster_id', types.Integer, schema.ForeignKey('clusters.id', ondelete="CASCADE")),
    schema.Column('score', types.Integer, default=0),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)

# Mobile Contact Table.
contacts_table = schema.Table('contacts', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('contacts_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('name', types.String(50)),
    schema.Column('contact_number', types.String(50)),
    schema.Column('is_invited', types.Boolean(), default=False),
    schema.Column('contact_our app_user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
    schema.Column('email', types.String(90), nullable=True), # TODO change its type to email

)

# Invite Contacts Table.
invite_contacts_table = schema.Table('invite_contacts', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('invite_contacts_seq_id', optional=True), primary_key=True),
    schema.Column('user_id', types.Integer, schema.ForeignKey('users.id', ondelete="CASCADE")),
    schema.Column('contact_number', types.String(50)),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),       
)

reserve_spot_table = schema.Table('reserve_spots', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('reserve_spots_seq_id', optional=True), primary_key=True),
    schema.Column('email', types.String(62), nullable=False), # TODO change its type to email
    schema.Column('cluster_id', types.Integer, schema.ForeignKey('clusters.id', ondelete="CASCADE")),
    schema.Column('expire_time', types.TIMESTAMP()),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
    schema.Column('is_remind', types.Boolean(), default=False),
    schema.Column('is_signed_up', types.Boolean(), default=False),
)

video_frames_table = schema.Table('video_frames', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('video_frames_seq_id', optional=True), primary_key=True),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('video_frame_key', types.String(255), nullable=True),
    schema.Column('video_frame_url', types.String(255), nullable=True),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)

promotions_emails_table = schema.Table('promotions_emails', metadata, 
    schema.Column('email', types.String(100), primary_key=True),
    schema.Column('sent_status', TINYINT(2), default=0),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)

beta_released_emails_table = schema.Table('beta_released_emails', metadata, 
    schema.Column('email', types.String(100), primary_key=True),
    schema.Column('username', types.String(100)),
    schema.Column('name', types.String(100)),
    schema.Column('sent_status', TINYINT(2), default=0),
    schema.Column('group_type', TINYINT(4), default=1),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
    schema.Column('sent_date', types.TIMESTAMP()),
)

invitation_email_table = schema.Table('invitation_emails', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('invitation_emails_seq_id', optional=True), primary_key=True),
    schema.Column('email', types.String(62), nullable=False), # TODO change its type to email
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)

cmj_artists_table = schema.Table('cmj_artists', metadata, 
    schema.Column('email', types.String(100), primary_key=True),
    schema.Column('username', types.String(50)),
    schema.Column('sent_status', TINYINT(2), default=0),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)

content_status_table = schema.Table('content_status', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('content_status_seq_id', optional=True), primary_key=True),
    schema.Column('content_id', types.Integer, schema.ForeignKey('contents.id', ondelete="CASCADE")),
    schema.Column('status', TINYINT(10), default=0),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)

# beta user tables
beta_user_table = schema.Table('beta_users', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('beta_users_seq_id', optional=True), primary_key=True),
    schema.Column('email', types.String(100), nullable=False),
    schema.Column('name', types.String(100), nullable=False),
    schema.Column('device_type', TINYINT(2), default=1),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)

beta_user_invitee_table = schema.Table('beta_user_invitees', metadata, 
    schema.Column('id', types.Integer,
        schema.Sequence('beta_user_invitees_seq_id', optional=True), primary_key=True),
    schema.Column('email', types.String(100), nullable=False),
    schema.Column('name', types.String(100), nullable=False),
    schema.Column('beta_user_id', types.Integer, schema.ForeignKey('beta_users.id', ondelete="CASCADE")),
    schema.Column('device_type', TINYINT(2), default=1),
    schema.Column('is_launch_complete', types.Boolean(), default=False),
    schema.Column('is_self_invitee', types.Boolean(), default=False),
    schema.Column('added_date', types.TIMESTAMP(), default=now()),
    schema.Column('modified_date', types.TIMESTAMP(), onupdate=now()),
)
