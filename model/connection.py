from model import models as model
from sqlalchemy import orm
from sqlalchemy.engine import Engine
from sqlalchemy import create_engine
from sqlalchemy import event
from lib import global_tools as gtools
from lib import constants
import time

# Create an engine and create all the tables we need
#engine = create_engine('mysql:///ourapp?use_unicode=0')

import logging

logging.basicConfig()
logger = logging.getLogger("myapp.sqltime")
logger.setLevel(logging.DEBUG)


'''
@event.listens_for(Engine, "before_cursor_execute")
def before_cursor_execute(conn, cursor, statement, parameters, context, executemany):
  context._query_start_time = time.time()
  logger.info("Start Query: %s" % statement)
  pass

@event.listens_for(Engine, "after_cursor_execute")
def after_cursor_execute(conn, cursor, statement, parameters, context, executemany):
  total = time.time() - context._query_start_time
  logger.info("Query Complete!")
  logger.info("Total Time: %f" % total)
  pass
'''

if gtools.is_staging():
  """When we use GAE then engine as:"""
  engine = create_engine('mysql+gaerdbms:///%s?instance=%s&charset=utf8'%(constants.STAGING_DATABASE, constants.SQLCLOUD_INSTANCE),
                 connect_args={"instance":constants.SQLCLOUD_INSTANCE})
elif gtools.is_prod():
  """When we use GAE then engine as:"""
  engine = create_engine('mysql+gaerdbms:///%s?instance=%s&charset=utf8'%(constants.LIVE_DATABASE, constants.SQLCLOUD_INSTANCE),
                 connect_args={"instance":constants.SQLCLOUD_INSTANCE})
else:
  engine = create_engine("mysql://root:123456@localhost/ourapp")
#event.listen(engine, "before_cursor_execute", before_cursor_execute)
#event.listen(engine, "after_cursor_execute", after_cursor_execute)

model.metadata.bind = engine
model.metadata.create_all()

# Set up the session
sm = orm.sessionmaker(bind=engine, autoflush=True, autocommit=False,
    expire_on_commit=False)
session = orm.scoped_session(sm)

def delete_handler(self):
    session.delete(self)
    #session.flush()
    session.commit()
    session.flush()
model.PostActivity.delete = delete_handler
model.PlaylistContent.delete = delete_handler
model.Content.delete = delete_handler
model.Album.delete = delete_handler
