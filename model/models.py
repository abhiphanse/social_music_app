#!/usr/bin/env python
from sqlalchemy import orm
from sqlalchemy.orm import column_property
from sqlalchemy import select, func
from sqlalchemy.sql import desc
from sqlalchemy.ext.hybrid import hybrid_property

from model.schema import *

class User(object):
  pass

class OAuthToken(object):
  pass

class FanList(object):
  pass

class Followers(object):
  pass

class ArtistData(object):
  pass

class ArtistImage(object):
  pass

class Content(object):
  __table__ = content_table
  pass

class Comment(object):
  pass

class Share(object):
  pass

class Like(object):
  pass

class Genre(object):
  pass

class Image(object):
  pass

class ProfileImage(object):
  pass

class Album(object):
  #pass
  @hybrid_property
  def content_count(self):
    result = select([func.count(content_table.c.id)]).where(content_table.c.album_id == self.id)
    return result
    #return len(self.children)

class Report(object):
  pass
  
class ContentPlayedHistory(object):
  pass

class Playlist(object):
  pass
   
class Party(object):
  pass

class HourlyContentStats(object):
  pass
    
class PlaylistSong(object):
  pass

class PlaylistContent(object):
  __table__ = playlist_content_table
  pass

class Post(object):
  pass

class PostActivity(object):
  __table__ = post_activity_table
  pass

class UserCommentVote(object):
  pass

class PartyPlaylists(object):
  pass

class PartyContentQueue(object):
  __table__ = party_content_queue_table
  pass

class PartyGuest(object):
  pass

class PartyQueue(object):
  pass

class GalleryAlbum(object):
  pass

class GalleryAlbumVote(object):
  pass

class SocialNetwork(object):
  __table__ = social_network_table
  pass

class VideoFrame(object):
  __table__ = video_frames_table
  pass

class SocialSharing(object):
  pass

class SupportIssue(object):
  pass

class VerificationCode(object):
  pass

class ReservationNumber(object):
  pass

class Cluster(object):
  pass

class ClusterUser(object):
  pass

class Contact(object):
  __table__ = contacts_table
  pass

class InviteContact(object):
  pass

class ReserveSpot(object):
  pass

class UserThread(object):
  pass

class ConversationThread(object):
  pass

class UserNotificationToggles(object):
  pass

class LibraryContent(object):
  __table__ = library_contents_table

class PromotionsEmails(object):
  __table__ = promotions_emails_table

class InvitationEmail(object):
  pass

class CmjArtist(object):
  __table__ = cmj_artists_table
  pass

class ContentStatus(object):
  __table__ = content_status_table
  pass

class BetaUser(object):
  pass

class BetaUserInvitee(object):
  __table__ = beta_user_invitee_table
  pass

class BetaReleasedEmails(object):
  __table__ = beta_released_emails_table
  pass

orm.mapper(User, user_table, properties={
    'content':orm.relation(Content, backref='user'),
    'images':orm.relation(Image, primaryjoin=user_table.c.image_id==image_table.c.id, backref='user' ),
    'blur_image':orm.relation(Image, primaryjoin=user_table.c.blur_image_id==image_table.c.id,),    
    'likes':orm.relation(Like, backref='user'),
    'comments':orm.relation(Comment, backref='user'),
    'cluster_user':orm.relation(ClusterUser, backref='user'),
    'cluster':orm.relation(Cluster, backref='user'),
})

orm.mapper(Content, content_table, properties={
    'comments':orm.relation(Comment, backref='content'),
    #'content_user':orm.relation(User, lazy="joined"),
    'album':orm.relation(
                          Album, 
                          backref=orm.backref('contents', order_by=content_table.c.sort_order)  
                         ),
    'genre':orm.relation(Genre, backref='content', lazy="joined"),
    'like':orm.relation(Like, backref='content'),
    'video_frames':orm.relation(VideoFrame),
    'content_status':orm.relation(ContentStatus, backref=orm.backref('content_status', order_by=desc(content_status_table.c.id))  )
})



orm.mapper(Comment, comment_table, properties={
    'votes':orm.relation(UserCommentVote, backref='comment')
})

orm.mapper(Share, share_table, properties={
    'content':orm.relation(Content),
    'user':orm.relation(User, primaryjoin=share_table.c.user_id==user_table.c.id ),
    'to_user':orm.relation(User, primaryjoin=share_table.c.to_user_id==user_table.c.id ),
})
orm.mapper(Like, like_table)
orm.mapper(OAuthToken, auth_token_table)
orm.mapper(FanList, fan_list_table)
orm.mapper(PromotionsEmails, promotions_emails_table)

orm.mapper(Followers, followers_table, properties={
    'follower':orm.relation(User, primaryjoin=followers_table.c.follower_id==user_table.c.id ),
    'following':orm.relation(User, primaryjoin=followers_table.c.user_id==user_table.c.id )
})

orm.mapper(Image, image_table)
orm.mapper(Album, album_table, properties={
    'images':orm.relation(Image, primaryjoin=album_table.c.image_id==image_table.c.id, backref='album'),
    'blur_image':orm.relation(Image, primaryjoin=album_table.c.blur_image_id==image_table.c.id),
    'user':orm.relation(User, backref='albums'),
})

orm.mapper(Genre, genre_table)

orm.mapper(ArtistImage, artist_image_table)
orm.mapper(ArtistData, artist_data_table, properties={
            'images' : orm.relation(Image, secondary=artist_image_table, order_by=image_table.c.sort_order)
                }
           )
           
orm.mapper(Playlist, playlist_table, properties={
    'user':orm.relation(User, backref=orm.backref('playlist')),
    'playlist_content':orm.relation(PlaylistContent),
    }
)
orm.mapper(Party, party_table, properties={
    'party_queue':orm.relation(PartyQueue, primaryjoin=party_table.c.id==party_queue_table.c.party_id),
    'user' : orm.relation(User)
})

orm.mapper(ContentPlayedHistory, content_played_history_table, properties={
    'content':orm.relation(Content)
})
orm.mapper(HourlyContentStats, hourly_content_stats_table, properties={
    'content':orm.relation(Content)
})

orm.mapper(Report, report_table, properties={
    'content':orm.relation(Content, backref='reports'),
    'reporter':orm.relation(User, primaryjoin=report_table.c.reporter_id==user_table.c.id ),
    'reportee':orm.relation(User, primaryjoin=report_table.c.reportee_id==user_table.c.id ),
    'post': orm.relation(Post),
})

orm.mapper(PlaylistContent, playlist_content_table, properties={
    'content':orm.relation(Content, order_by=content_table.c.added_date)
})
orm.mapper(Post, post_table, properties={
    'user' : orm.relation(User, primaryjoin=user_table.c.id==post_table.c.user_id),
    'shared_user' : orm.relation(User, primaryjoin=user_table.c.id==post_table.c.shared_user_id ),
    'content' : orm.relation(Content),
    'playlist' : orm.relation(Playlist),
    'party': orm.relation(Party),
    'gallery' : orm.relation(GalleryAlbumVote, primaryjoin=gallery_album_vote_table.c.id==post_table.c.gallery_vote_id),
})

orm.mapper(PostActivity , post_activity_table,
    properties={
      'post': orm.relation(Post),
      'user': orm.relation(User, primaryjoin=user_table.c.id==post_activity_table.c.user_id, ),
      'post_user' : orm.relation(User, primaryjoin=user_table.c.id==post_activity_table.c.post_user_id),
      'follower' : orm.relation(User, primaryjoin=user_table.c.id==post_activity_table.c.follower_id),
      'content':orm.relation(Content),
    })
    
orm.mapper(UserCommentVote, user_comment_vote_table)

orm.mapper(PartyPlaylists , party_playlist_table)

orm.mapper(GalleryAlbumVote , gallery_album_vote_table, properties={
    'album':orm.relation(Album, lazy="joined"),
    'user':orm.relation(User, lazy="joined"),
})

orm.mapper(GalleryAlbum , gallery_albums_table, properties={
    'album':orm.relation(Album, backref="album_gallery"),
    'album_vote':orm.relation(GalleryAlbumVote)
})

orm.mapper(PartyContentQueue, party_content_queue_table, properties={
      'user': orm.relation(User ),
      'content': orm.relation(Content ),
    })

orm.mapper(PartyGuest, party_guest_table, properties={
      'guest_user': orm.relation(User),
    })

orm.mapper(PartyQueue, party_queue_table)
orm.mapper(SocialNetwork, social_network_table, properties={
      'friend_our app_user':orm.relation(User, primaryjoin=user_table.c.id==social_network_table.c.friend_our app_user_id ),
    })
orm.mapper(ProfileImage, profile_image_table)
orm.mapper(SocialSharing, social_sharing_table)

orm.mapper(SupportIssue, support_issue_table)

orm.mapper(VerificationCode, verification_codes_table)
orm.mapper(ReservationNumber, reservation_numbers_table)
orm.mapper(Cluster, clusters_table)

orm.mapper(ClusterUser, cluster_users_table, properties={
     'cluster':orm.relation(Cluster),
    } )

orm.mapper(InviteContact, invite_contacts_table)
orm.mapper(ReserveSpot, reserve_spot_table)
orm.mapper(InvitationEmail, invitation_email_table)
orm.mapper(CmjArtist, cmj_artists_table)
orm.mapper(BetaReleasedEmails, beta_released_emails_table)

orm.mapper(Contact, contacts_table, properties={
     'user':orm.relation(User, primaryjoin=user_table.c.id==contacts_table.c.user_id ),
     'contact_our app_user':orm.relation(User, primaryjoin=user_table.c.id==contacts_table.c.contact_our app_user_id ),
    })

orm.mapper(UserThread, user_thread_table, properties={
     'user_1':orm.relation(User, primaryjoin=user_table.c.id==user_thread_table.c.user_1_id ),
     'user_2':orm.relation(User, primaryjoin=user_table.c.id==user_thread_table.c.user_2_id ),
    } )
orm.mapper(ConversationThread, conversation_thread_table, properties={
     'post_activity':orm.relation(PostActivity),
    } )

orm.mapper(UserNotificationToggles, user_notification_toggles_table, properties={
     'user':orm.relation(User, backref="notification_toggles"),
    } )

orm.mapper(LibraryContent, library_contents_table, properties={
     'content':orm.relation(Content, backref="library_content"),
     'user':orm.relation(User),
    })
orm.mapper(VideoFrame, video_frames_table)
orm.mapper(ContentStatus, content_status_table)
orm.mapper(BetaUser, beta_user_table)
orm.mapper(BetaUserInvitee, beta_user_invitee_table, properties={
     'invitor':orm.relation(BetaUser, backref="invitees"),
    })
import model_event_binder
