# python imports
import datetime
import time
import logging
import os

# django imports
from django.utils.encoding import smart_str
from django.utils.hashcompat import md5_constructor
from django.utils.hashcompat import sha_constructor
from django.utils.translation import ugettext_lazy as _
from random import choice

def make_random_token(length=30, allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789'):
  "Generates a random token with the given length and given allowed_chars"
  # Note that default value of allowed_chars does not have "I" or letters
  # that look like it -- just to avoid confusion.
  return ''.join([choice(allowed_chars) for i in range(length)])

def get_hexdigest(algorithm, salt, raw_password):
  """
  Returns a string of the hexdigest of the given plaintext password and salt
  using the given algorithm ('md5', 'sha1' or 'crypt').
  """
  raw_password, salt = smart_str(raw_password), smart_str(salt)
  if algorithm == 'crypt':
    try:
      import crypt
    except ImportError:
      raise ValueError('"crypt" password algorithm not supported in this environment')
    return crypt.crypt(raw_password, salt)

  if algorithm == 'md5':
    return md5_constructor(salt + raw_password).hexdigest()
  elif algorithm == 'sha1':
    return sha_constructor(salt + raw_password).hexdigest()
  raise ValueError("Got unknown password algorithm type in password.")


def encrypt_password(raw_password):
  if raw_password is None:
    # Sets a value that will never be a valid hash
    return '!'
  else:
    import random
    algo = 'sha1'
    salt = get_hexdigest(algo, str(random.random()),
                         str(random.random()))[:5]
    hsh = get_hexdigest(algo, salt, raw_password)
    return '%s$%s$%s' % (algo, salt, hsh)

def check_password(raw_password, enc_password):
  """
  Returns a boolean of whether the raw_password was correct. Handles
  encryption formats behind the scenes.
  """
  algo, salt, hsh = enc_password.split('$')
  return hsh == get_hexdigest(algo, salt, raw_password)
