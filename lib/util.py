# Python imports
import datetime
import logging
import string
import random
import math
import os
import re
import sys
import base64, hmac, sha
import json
import time
# dajngo imports
from django.template import Context
from django.template.loader import get_template

# GAE import
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.api import memcache
from google.appengine.api import taskqueue
from google.appengine.ext import blobstore
from google.appengine.api import images
from google.appengine.api.images import NotImageError
from google.appengine.api import urlfetch
from google.appengine.api import images

# Django imports.
from django.utils import simplejson

# local import
from lib import constants

from lib import global_constants as gconst
from lib import global_tools as gtools
from lib import gaesessions
from lib import auth
from lib import push_notification
from lib import plivo
from lib import plivo_util
from lib import sendgrid
#model import.
from model.connection import session
from model import models as model
from model.schema import *

#sqlalchemy imports.
from sqlalchemy.sql import desc
from sqlalchemy import update
from sqlalchemy import delete
from sqlalchemy.sql import and_
from sqlalchemy.orm import load_only

"""  All generic tools moved to /lib/global_tools.py.
     Please put non-application-specific utility methods there.
"""

def render_to_string(template_name, template_values=None):
  """ Renders a django template to a string given a value dict """
  template_values = template_values or {}
  if 'http_domain' not in template_values:
    template_values['http_domain'] = get_http_domain()
  if 'ssl_domain' not in template_values:
    template_values['ssl_domain'] = get_ssl_domain()
  return get_template(template_name).render(Context(template_values))

def get_active_user(use_cache=True, load_only_params=True):
  user = None
  session = gaesessions.get_current_session()
  if session.has_key('user_id'):
    if gaesessions.get_is_password_change():
      return None
    if not user:
      if load_only_params:
        user = get_user_by_id(session['user_id'], load_only_params=True)
      else:
        user = get_user_by_id(session['user_id'], load_only_params=False)
      if user:
        if not user.is_active:
          if session.is_active():
            session.terminate()
            user = None          
  return user if user else None

def delete_active_session(user):
  session = gaesessions.get_current_session()
  if user:
    if session.has_key('user_id'):
      if session.is_active():
        session.terminate()
          
def get_http_domain(appspot_required=False):
  if appspot_required or gtools.is_staging() or gtools.is_dev():
    return 'http://%s' % gtools.get_host()
  else:
    return 'http://' + constants.VM_DOMAIN

def get_domain(appspot_required=False):
  if appspot_required or gtools.is_staging() or gtools.is_dev():
    return 'http://%s' % gtools.get_host()
  else:
    return 'http://' + constants.Live_VM_DOMAIN

def get_ssl_domain(appspot_required=False):
  if appspot_required or gtools.is_dev():
    # If we're on a versioned appspot domain or dev there is no ssl
    return 'http://%s' % gtools.get_host()
  if gtools.is_staging():
    # The default version of staging has SSL
    return 'http://%s' % gtools.get_host()
  
  if gtools.is_proxy():
    # https proxying not working for login in charles with new sessions
    return 'http://' + constants.VM_DOMAIN
  return 'https://' + constants.VM_DOMAIN


def get_signup_url():
  return get_http_domain() + '/signup'


def get_login_url():
  return get_ssl_domain() + '/login'


def get_logout_url():
  return get_http_domain() + '/logout'


def get_userinfo_url():
  return get_http_domain() + '/userinfo'


def validate_email(email):
  if re.match('^([!|\#|$|%|^|&|*|(|)|_|+|\-|=|~|`|{|}|[|\]|.]*\w+[!|\#|$|%|^|&|*|(|)|_|+|\-|=|~|`|{|}|[|\]|.]*)+[@]([!|\#|$|%|^|&|*|(|)|_|+|\-|=|~|`|{|}|[|\]|.]*\w+[!|\#|$|%|^|&|*|(|)|_|+|\-|=|~|`|{|}|[|\]|.]*)+[.](\w+$)', email):
    return True
  else :
    return False

def validate_username(username):
  if re.match("^[a-zA-Z0-9_]*$", username):
    return True
  else:
    return False
    
def validate_contact_number(contact_number):
  if re.match("^[0-9]\d*$", contact_number):
    return True
  else:
    return False
    
def filter_contact_number(contact_number):
  trimed_number = contact_number.replace(" ", "")
  new_number = re.sub('[(){}<>-]', '', trimed_number)
  return new_number

def send_welcome_email(user, deactive_url, verify_url):
  task_params = {
      'user_id': user.id,
      'deactive_url': deactive_url,
      'verify_url':verify_url,
  }
  taskqueue.add(url='/tasks/send_welcome_email',
                queue_name='email',
                params=task_params)

def send_invitaion_mail(email, user, is_exist, exist_user_name=None):
  task_params = {
      'is_exist':is_exist,
      'email': email,
      'exist_user_name':exist_user_name ,
      'user_id' : user.id
      }
  
  taskqueue.add(url='/tasks/send_invitation_mail',
                queue_name='email',
                params=task_params)

def send_admin_delete_mail(user):
  task_params = {
      'user_id': user.id,
      }
  
  taskqueue.add(url='/tasks/send_admin_delete_mail',
                queue_name='email',
                params=task_params)

def reset_password_email(user, password_reset_url):
  task_params = {
        'password_reset_url' : password_reset_url,
        'user_id':  user.id,
      }
  
  taskqueue.add(url='/tasks/reset_password_email',
                queue_name='email',
                params=task_params)


def reset_username_email(user):
  task_params = {
      'user_id': user.id,
      }
  
  taskqueue.add(url='/tasks/reset_username_email',
                queue_name='email',
                params=task_params)

def login(email, password, device_id, device_type=None, ios_env=None):
  # Close any active session the user has since he is trying to login.
  session = gaesessions.get_current_session()
  if session.is_active():
    session.terminate()

  user =  get_user_by_email(email)
  if not user:
    user = get_user_by_name(email)
  # Does the user exist and does the password match?
  if user and auth.check_password(password, user.password):
    logging.info( " device_type  %s  "%device_type  )
    session['user_id'] = user.id
    session['device_id'] = device_id
    session['device_type'] = device_type
    session['ios_env'] = ios_env
    session.save()
    return user

  return None

def get_user_by_name(name):
  user = session.query(model.User).filter(model.User.username == name).first()
  return user
  
def get_user_by_email(email):
  user = session.query(model.User).filter(model.User.email == email).first()
  return user

def get_beta_user_by_email(email):
  beta_user = session.query(model.BetaUser).filter(model.BetaUser.email == email).first()
  return beta_user

def get_beta_invitee_by_id(beta_user_id):
  beta_invitee = session.query(model.BetaUserInvitee).filter(model.BetaUserInvitee.id == beta_user_id).first()
  return beta_invitee

def get_beta_invitee_by_email(email):
  beta_invitee = session.query(model.BetaUserInvitee).filter(model.BetaUserInvitee.email == email).first()
  return beta_invitee

def get_user_by_id(user_id, load_only_params=None):
  if not load_only_params:
    user = session.query(model.User).filter(model.User.id == user_id).first()
  else:
    logging.info( load_only_params)
    user = session.query(model.User).\
                  options(
                    load_only('id', 'username', 'name', 'user_type', 'image_id','is_active', 'following_count', 'follower_count', 's3_image_key', 's3_blur_image_key'),
                  ).\
                  filter(model.User.id == user_id).first()

  return user
  
def deactived_users():
  user = session.query(model.User).filter(model.User.is_active == 0)
  return user

def random_number():
  chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
  size = random.randint(8, 12)
  return ''.join(random.choice(chars) for x in range(size))

def get_policy_and_signature(params={}):
  
  key = get_unique_key()
  #success_redirect = "" TODO in future we can use suuccess redirect
  
  meta_uuid = gtools.dt_to_timestamp(datetime.datetime.now())

  policy = get_policy_dict(key=key, meta_uuid = meta_uuid)
  policy_encoded = base64.b64encode(policy)
  signature = base64.b64encode(hmac.new(constants.AWS_SECRET_ACCESS_KEY, policy_encoded, sha).digest())
  params = {
    'policy' : policy_encoded,
    'signature' : signature,
    'key' : key,
    'AWSAccessKeyId' : constants.AWS_ACCESS_KEY_ID,
    'meta_uuid' : meta_uuid,
  }
  return params
  
def get_policy_dict(key, meta_uuid ):
  expiry_after = datetime.datetime.now() + datetime.timedelta(hours = 12 )
  policy_dict ={ "expiration": "2025-12-01T12:00:00.000Z",
    "conditions": [
      {"bucket": constants.DEFAULT_BUCKET_NAME },
      ["starts-with", "$key", key],
      {"acl": "public-read" },
      #{"success_action_redirect": str(success_redirect) },
      ["starts-with", "$Content-Type", "audio/mpeg"],
      {"success_action_status" : "201" },
      {"x-amz-meta-uuid": str(meta_uuid) },
      ["starts-with", "$x-amz-meta-tag", ""],
    ]
  }
  return json.dumps(policy_dict)

def get_unique_key(params={}):
  timestamp = str(long( time.time() * 1000 ))
  return timestamp

def get_host_name():
  return os.environ.get('HTTP_HOST')

def send_email_for_delete_confirmation(user, confirmation_url, cancel_url, content_id=None, album_id=None):
  task_params = {
        'user_id':  user.id,
        "confirmation_url" : confirmation_url,
        "cancel_url" : cancel_url,
      }
  
  if album_id:
    task_params['album_id'] = album_id
  if content_id :
    task_params['content_id'] = content_id
  taskqueue.add(url='/tasks/delete_confirmation',
                queue_name='email',
                params=task_params)

def get_full_image_by_id(image_id):
  image_url = None
  try:
    if image_id:
      image = session.query(model.Image).filter(model.Image.id==image_id).first()
      if image and image.full_file_loc:
        image_url = "http://%s/images/profile/1?blob_key=%s"%(gtools.get_host(), image.full_file_loc)
  except Exception,e:
    logging.info(e)
    pass
  return image_url

def get_image_url(full_file_loc):
  image_url = None
  if full_file_loc:
    image_url = "http://%s/images/profile/1?blob_key=%s"%(gtools.get_host(), full_file_loc)

  return image_url

def get_content_by_id(content_id):
  content = session.query( model.Content ).\
                            filter( model.Content.id == content_id).first()
  return content

def get_post_by_id(post_id):
  post = session.query( model.Post ).\
                            filter( model.Post.id == post_id).first()
  return post

def get_post_activity_by_id(post_activity_id):
  post = session.query( model.PostActivity ).\
                            filter( model.PostActivity.id == post_activity_id).first()
  return post

def stream_content(content, user_id, party_id=None, stream_by=None):
  response = {}  
  if content and user_id:
    duration = int(content.duration) if content.duration else 270  # get_content_duration(content_id)
    start_time = datetime.datetime.now()
    end_time = start_time + datetime.timedelta(seconds=int(duration))

    stream = model.ContentPlayedHistory()
    stream.content_id = content.id
    stream.user_id = user_id
    stream.party_id = party_id
    stream.stream_by = stream_by
    stream.start_time = start_time
    stream.end_time = end_time
    session.add(stream)
    session.commit()

  content.play_count = content.play_count + 1
  content.current_playing_score = float(content.current_playing_score) + float(constants.CURRENT_PLAYING_COUNT_WEIGHTAGE)
  content.real_time_score = float(content.real_time_score) + float(constants.PLAY_COUNT_WEIGHTAGE)
  session.merge(content)
  session.commit()

  """
  update_content = update(model.Content).where(model.Content.id==content.id).\
            values(play_count=model.Content.play_count+1).\
            values(current_playing_score=model.Content.current_playing_score+constants.CURRENT_PLAYING_COUNT_WEIGHTAGE).\
            values(real_time_score=model.Content.real_time_score+constants.PLAY_COUNT_WEIGHTAGE)


  result =  session.execute(update_content)
  session.commit()
  """

  response['status'] = True
  response['data'] = {'msg':'content has been played.'}

  return response

def get_content_duration(content_id):
  duration = 270
  content = session.query(model.Content.duration).filter(model.Content.id==content_id).first()
  if content and content.duration:
    duration = int(content.duration)
  return duration
  
def unlike_content(user_id, content_id):
  response = {}
  like_obj = session.query(model.Like).\
                          filter(model.Like.content_id == content_id).\
                          filter(model.Like.user_id == user_id).first()
  if like_obj :

    like_obj = session.query(model.Like).\
                            filter(model.Like.content_id == content_id).\
                            filter(model.Like.user_id == user_id)       
    like_obj.delete()
  
    update_content = update(model.Content).where(model.Content.id==content_id).\
              values(like_count=model.Content.like_count-1)
    result =  session.execute(update_content)
    logging.info( result )
  
    session.commit()
    response['status'] = True
    response['data'] = {'msg':'content has been unliked.'}
  else:
    response['status'] = False
    response['errors'] = {'error':'content has not been liked yet.'}
  return response
  
def get_thumb_image_by_id(image_id):
  image_url = None
  try:
    if image_id:
      image = session.query(model.Image).filter(model.Image.id==image_id)
      if image:
        blob_key = image[0].thumb_file_loc
        image_url = images.get_serving_url(blob_key)
  except Exception,e:
    logging.info(e)
    pass
    
  return image_url
  
def update_comment(user_id, vote, comment):
  response = {
      'status' : constants.ERROR,
      'data' : None,
      'errors' : None
  }
  logging.info(comment)
  if not comment:
    response['errors'] = {"error":constants.INVALID_VALUE}
    return response
  update_comment_vote(user_id, vote, comment)
  response['status'] = constants.SUCCESS
  response['data'] = {'comment_id':comment.id}
  return response

def update_comment_vote(user_id, vote, comment):
  response = {
              'status' : constants.ERROR
  }
  vote_count = 0
  comment_vote = session.query(model.UserCommentVote).filter(model.UserCommentVote.comment_id == comment.id).\
                                   filter(model.UserCommentVote.user_id == user_id).first()
  if comment_vote and comment_vote.vote_type == vote:
    return response
  else: 
    if not comment_vote:
      comment_vote = model.UserCommentVote()
      comment_vote.user_id = user_id
      comment_vote.vote_type = vote
      comment_vote.comment_id = comment.id
      session.add(comment_vote)
      session.flush()
      comment.vote = comment.vote + vote
      session.merge(comment)
      session.commit()
    else:
      if comment_vote.vote_type == 1:
        if vote == -1:
          comment.vote = comment.vote-2
        elif vote == 0:
          comment.vote = comment.vote-1
      elif comment_vote.vote_type == -1:
        if vote == 0:
          comment.vote = comment.vote+1
        elif vote == 1:
          comment.vote = comment.vote+2
      elif comment_vote.vote_type == 0:
        comment.vote = comment.vote + vote
      comment_vote.vote_type = vote
      session.merge(comment)
      session.merge(comment_vote)
      session.commit()
    response['status'] = constants.SUCCESS
  return response

def set_content_duration(content_id):
  content = get_content_by_id(int(content_id)) if content_id else None
  if content:
    try:
      urlfetch.set_default_fetch_deadline(60)
      content_seconds = 0
      content_url = constants.EC2_SERVER_URL +"/duration?s3_url=%s"%content.s3_location
      logging.info( content_url )
      response = urlfetch.fetch(content_url)
      if response:
        logging.info(response.content)
        json_response = json.loads(response.content)
        content_seconds = json_response['sec']
        logging.info(content_seconds)
      if content_seconds:
        content.duration = int(content_seconds)
        session.commit()
        send_request_for_content_processing(content)
        return content
    except Exception as e:
      logging.exception(e)
      set_error_status_for_content_processing(content)
      support_email_for_content_processing_error(content, "Failed during duration calculation.")

  return None

def send_request_for_content_processing(content):
  task_params = {'content_id': content.id}
  logging.info("sending reuquest for converting content.")
  taskqueue.add(url='/tasks/convert_media_codec',
            method='GET',
            queue_name='optimize-queue',
            params= task_params)

def set_content_cover_frame(content_id):
  if content_id:
    logging.info("fetching video cover frame")
    content = get_content_by_id(int(content_id))
    logging.info(content.content_type)
    if content.content_type == 1:
      s3_frame_bucket = constants.CONTENT_FRAME_BUCKET
      frame_key = content.s3_Key+"_frame.png"
      content_url = constants.EC2_INSTANCE_URL+"/cover_frame?s3_url=%s&s3_bucket=%s&frame_key=%s"%(content.s3_location, s3_frame_bucket, frame_key)
      urlfetch.set_default_fetch_deadline(50)
      response = urlfetch.fetch(content_url)
      if response:
        logging.info(response.content)
        json_response = json.loads(response.content)
        if json_response['s3_frame_url']:
          content.s3_frame_url = json_response['s3_frame_url']
          content.s3_frame_key = json_response['s3_frame_key']
          content = session.merge(content)
          session.commit()

def get_followers(user_id=None):
  follower_ids = []
  if not user_id:
    user = get_active_user()
    user_id = user.id

  following_list = session.query(model.Followers.user_id).filter(model.Followers.follower_id==user_id)
  follower_ids = [int(uer[0]) for uer in following_list ]
  return follower_ids

def get_followers_ids(user=None):
  follower_ids = []
  if not user:
    user = get_active_user()
  if user:
    following_list = session.query(model.Followers.user_id).filter(model.Followers.follower_id==user.id)
    follower_ids = [int(uer[0]) for uer in following_list ]
  return follower_ids
  
def get_comment_vote_type(comment_id):
  vote_type = 0
  user = get_active_user()
  if user:
    comment_vote = session.query(model.UserCommentVote).filter(model.UserCommentVote.comment_id == comment_id).\
                                               filter(model.UserCommentVote.user_id == user.id).first()
    if comment_vote:
       vote_type = comment_vote.vote_type
  return vote_type
  
def get_playlist_by_id(playlist_id):
  playlist = session.query( model.Playlist ).\
                            filter( model.Playlist.id == playlist_id).first()
  return playlist

def get_artist_page_by_id(page_id):
  if not page_id: return None
  artist_page = session.query(model.ArtistData).filter(model.ArtistData.id == int(page_id)).first()
  return artist_page

def rescale(img_data, width, height, halign='middle', valign='middle'):
  """Resize then optionally crop a given image.

  Attributes:
    img_data: The image data
    width: The desired width
    height: The desired height
    halign: Acts like photoshop's 'Canvas Size' function, horizontally
            aligning the crop to left, middle or right
    valign: Verticallly aligns the crop to top, middle or bottom

  """
  image = images.Image(img_data)

  desired_wh_ratio = float(width) / float(height)
  wh_ratio = float(image.width) / float(image.height)

  if desired_wh_ratio > wh_ratio:
    # resize to width, then crop to height
    image.resize(width=width)
    image.execute_transforms()
    trim_y = (float(image.height - height) / 2) / image.height
    if valign == 'top':
      image.crop(0.0, 0.0, 1.0, 1 - (2 * trim_y))
    elif valign == 'bottom':
      image.crop(0.0, (2 * trim_y), 1.0, 1.0)
    else:
      image.crop(0.0, trim_y, 1.0, 1 - trim_y)
  else:
    # resize to height, then crop to width
    image.resize(height=height)
    image.execute_transforms()
    trim_x = (float(image.width - width) / 2) / image.width
    if halign == 'left':
      image.crop(0.0, 0.0, 1 - (2 * trim_x), 1.0)
    elif halign == 'right':
      image.crop((2 * trim_x), 0.0, 1.0, 1.0)
    else:
      image.crop(trim_x, 0.0, 1 - trim_x, 1.0)

  return image.execute_transforms()

def get_party_by_id(party_id):
  party = session.query( model.Party ).\
                            filter( model.Party.id == party_id).first()
  return party

def validate_passphrase(party, passcode):
  party = session.query(model.Party).filter( and_(model.Party.id==party.id,model.Party.passcode==passcode) ).first()
  return True if party else False

def get_party_queue(party, queue_type):
  queue = None
  for party_queue in party.party_queue:
    if party_queue.queue_type == queue_type:
      queue = party_queue

  return queue

def get_ios_environment(device_type, request):
  ios_environment = None
  if device_type == "ios":
    agent = gtools.get_user_agent(request)
    logging.info("User Agent ::  %s"%agent)
    if "com.our app.our app" in agent:
      ios_environment = constants.APPLE_ENVIRONMENT
    elif "com.Distribution.our app" in agent:
      ios_environment = constants.DISTRIBUTION_ENVIRONMENT
    elif "com.Development.our app" in agent:
      ios_environment = constants.DEVELOPMENT_ENVIRONMENT
    else:
      ios_environment = constants.DEVELOPMENT_ENVIRONMENT
  return ios_environment

def get_device_type(request):
  device_type = None
  agent = gtools.get_user_agent(request)
  logging.info("User Agent ::  %s"%agent)
  if gtools.is_android(agent):
    device_type = "android"
  elif gtools.is_ios(agent):
    device_type = "ios"
  return device_type
  
def is_current_user(specify_user_id):
  session = gaesessions.get_current_session()
  return True if int(session['user_id'])  == int(specify_user_id) else False

def is_following(follower_ids, user_id):
  is_following_value = False
  if user_id in follower_ids:
    is_following_value = True
  return is_following_value

def notification_to_device(sender_id, receiver_ids, notification_type, message=None):
  notification_params = {
        'sender_id' : sender_id,
        'receiver_ids' : receiver_ids,
        'message' : message,
        'notification_type' : notification_type,
  }
  taskqueue.add(queue_name='send-notifiation-to-device',
                url='/tasks/send_notifiation_to_device',
                params=notification_params)

def send_notification_to_device(receiver_ids, sender_id, notification_params ):
  logging.info(receiver_ids)
  logging.info(notification_params)
  devices = get_device_ids(receiver_ids)
  for device in devices:
    if device.device_id and device.device_type :
      push_notification.push_notification(device.device_id, device.device_type, notification_params, ios_env=device.ios_env)

def get_device_ids(receiver_ids, device_id=None):
  
  receiver_ids_chunks = [receiver_ids[x:x+15] for x in xrange(0, len(receiver_ids), 15)]
  active_user_session = []
  for ids in receiver_ids_chunks:
    active_user_session.extend( gaesessions.SessionModel().all().filter('user_id  IN', ids).filter("device_id !=",None) )
  
  #active_user_session = gaesessions.SessionModel().all().filter('user_id  IN', receiver_ids).filter("device_id !=",device_id)
  return active_user_session
    
def domain_name():
  
  domain_name = "http://our appstaging.appspot.com"
  if gtools.is_prod():
    domain_name = "http://our appapp.appspot.com"
  # domain_name = "http://%s"%gtools.get_host()
  return domain_name

def get_social_networks_ids(user, network_type = "facebook"):
  social_networks = session.query(model.SocialNetwork.network_id).filter(model.SocialNetwork.user_id == user.id).\
                                                      filter(model.SocialNetwork.network_type == network_type).all()
  network_ids = [int(social_network.network_id) for social_network in social_networks]
  return network_ids
  
def content_validation(user, content_id=None, content=None):
  response = {
      'data':None,
      'status':True,
      'errors':None
  }
  logging.info(content_id)
  logging.info(content)
  try:
    if content_id and not content:
      content_id = base64.standard_b64decode(str(content_id))
      content = get_content_by_id(content_id)
    if not (content and content.user_id == user.id and content.content_type == 1):
      response['errors'] = {"error":"Invalid Content ID."}
      response['status'] = False 
    else:
      response['data'] = {'content': content}
  except TypeError:
    response['errors'] = {"error":"Invalid Content ID."}
    response['status'] = False
  return response
  
def get_album_image_url(full_file_loc = None):
  image_url = None
  if full_file_loc:
    image_url = "http://%s/images/profile/0?blob_key=%s"%(gtools.get_host(), full_file_loc)
  return image_url
  
def get_streaming_url(content, device_type, protocol):
  bucket_name = constants.DEFAULT_BUCKET_NAME

  split_s3_key = str(content.s3_Key).split(".") if device_type == constants.DEVICE_ANDROID else str(content.s3_Key_before_convert).split(".")
  c_format = str(split_s3_key[-1]).lower()
  logging.info("content url format %s" %c_format)
  logging.info("content format %s" %content.format)
  content_category = "mp3" if c_format == "mp3" else "mp4"

  if content.content_type == constants.VIDEO:
    s3_key = content.s3_Key
  else:
    s3_key = content.s3_Key if device_type == constants.DEVICE_ANDROID else content.s3_Key_before_convert

  content_url = "%s://%s/vods3/_definst_/%s:amazons3/%s/%s" %(protocol, constants.EC2_DOMAIN_URL, content_category, bucket_name, s3_key)
  if device_type == constants.DEVICE_IPHONE and protocol == 'http':
    content_url =  content_url + "/playlist.m3u8"
  
  return content_url


def get_album_by_id(album_id):
  album = session.query( model.Album ).\
                            filter( model.Album.id == album_id).first()
  return album

def get_comment_by_id(comment_id):
  comment = session.query( model.Comment ).\
                            filter( model.Comment.id == comment_id).first()
  return comment  

def update_in_media_cache(content):
  urlfetch.set_default_fetch_deadline(40)

  if str(content.format).lower() == "mp3":  
    content_category = 'mp3'
  else:  
    content_category = 'mp4'    

  instance_url = "http://%s:8084"%constants.EC2_DOMAIN_URL

  media_server_url = "%s/update_wowza_media_cache/?s3_key=%s&bucket=%s&storage_server=amazons3&file_format=%s"%(instance_url, content.s3_Key, content.buckets, content_category )
  logging.info(media_server_url)
  response = urlfetch.fetch(media_server_url)
  logging.info( response )
  return
  
def save_profile_image(user):
  random_number = random.randint(0, len(constants.USER_DEFAULT_IMAGES)-1 )
  image_key = constants.USER_DEFAULT_IMAGES[random_number]
  user.s3_image_key = image_key
  user.s3_compressed_image_key = "compressed_" + str(image_key)
  user.s3_rescale_image_key = "rescale_" + str(image_key)
  session.merge(user)
  session.commit()
  return user
    
def get_user_image_url(full_file_loc = None):
  image_url = None
  if full_file_loc:
    image_url = "http://%s/images/profile/0?blob_key=%s"%(gtools.get_host(), full_file_loc)
  return image_url

def send_mobile_sms(contact_number, message):
  try:
    plivo_api = plivo.RestAPI(constants.PLIVO_AUTH_ID, constants.PLIVO_AUTH_TOKEN)
    caller_id = get_caller_id()
    #caller_id = constants.PLIVO_CALLER_ID
    logging.info("caller id : %s"%caller_id)
    # Send a SMS
    params = {
        'src': caller_id, # Caller Id
        'dst' : contact_number, # User Number to Call
        'text' : message,
        'type' : "sms",
    }
    response = plivo_api.send_message(params)
    logging.info("######## Sent SMS Response   ########")
    logging.info(response)
    
    get_message_params = {"record_id":str(response[1]["message_uuid"][0])}
    time.sleep(5)
    response = plivo_api.get_message(get_message_params)
    logging.info("######## get message response   ########")
    logging.info(response)

  except Exception as e:
    logging.info(e)

def get_user_by_contact_number(contact_number, existing_users):
  real_user = None
  for user in existing_users:
    if user.full_contact_number == contact_number:
      real_user = user
  return real_user


def send_notification_to_self_device(receiver_ids, notification_params, device_id=None):
  logging.info("---------  send notification ------------------------------------")
  logging.info(notification_params)
  devices = get_device_ids(receiver_ids, device_id=device_id)
  for device in devices:
    if device.device_id and device.device_type:
      push_notification.push_notification(device.device_id, device.device_type, notification_params, ios_env=device.ios_env)

def artist_count():
  artists = session.query(model.User.id).filter(model.User.user_type == constants.ARTIST_TYPE).all()
  return len(artists)

def get_cluster_position(cluster_id):
  clusters = session.query(model.Cluster.id, model.Cluster.user_count).order_by(desc(model.Cluster.user_count)).\
                                order_by(desc(model.Cluster.total_score)).\
                                order_by(model.Cluster.added_date).all()
  cluster_list = [int(cluster.id) for cluster in clusters]
  return cluster_list.index(cluster_id)+1

def get_s3_host_url(bucket):
  s3_host_url = 'https://{bucket}.{host}'.format(
            host=constants.S3_HOST_NAME,
            bucket=bucket)
  return s3_host_url

def get_launch_video_url(device_type):
  video_url_protocol = "rtsp" if device_type == "android" else "http"
  launch_video_url = video_url_protocol + "://"+ constants.EC2_DOMAIN_URL + constants.LAUNCH_VIDEO_URL
  if device_type == "ios":
    launch_video_url = launch_video_url + "/playlist.m3u8"
  return launch_video_url

def send_request_for_convert_content(content_id):
  content = get_content_by_id(int(content_id)) if content_id else None
  if content:
    try:
      request_url = constants.EC2_SERVER_URL +"/convert_media?key=%s&bucket=%s&location=%s&env=%s&content_id=%s&content_type=%s"%(content.s3_Key, content.buckets, content.s3_location, gtools.get_env(), str(content.id), str(content.content_type))
      logging.info(request_url)
      response = urlfetch.fetch(request_url)
      logging.info(response)
    except Exception as e:
      logging.exception(e)
      set_error_status_for_content_processing(content)
      support_email_for_content_processing_error(content, "Failed during sending request for content conversion.")
  return

def send_request_to_create_cover_frames(content_id):
  content = get_content_by_id(int(content_id)) if content_id else None
  if content:
    try:
      logging.info("sending request to ec2 to create video frame")
      if not content.duration:
        urlfetch.set_default_fetch_deadline(60)
        content_seconds = 0
        content_url = constants.EC2_SERVER_URL +"/duration?s3_url=%s"%content.s3_location
        logging.info( content_url )
        response = urlfetch.fetch(content_url)
        if response:
          logging.info(response.content)
          json_response = json.loads(response.content)
          content_seconds = json_response['sec']
          logging.info(content_seconds)
          if content_seconds:
            content.duration = int(content_seconds)
            session.commit()

      time_slot = int(math.floor(content.duration/10))
      request_url = constants.EC2_SERVER_URL +"/cover_frame?time_slot=%s&video_url=%s&bucket=%s&env=%s&content_id=%s" %(time_slot, content.s3_location, constants.CONTENT_FRAME_BUCKET, gtools.get_env(), str(content.id))
      logging.info(request_url)
      response = urlfetch.fetch(request_url)
      logging.info(response)
    except Exception as e:
      logging.exception(e)
      support_email_for_content_processing_error(content, "Failed during sending request for extracting video frames.")
  return

def save_deafult_cover_frame_for_content(content_id):
  content = get_content_by_id(int(content_id)) if content_id else None
  if content:
    try:
      default_video_frame = session.query(model.VideoFrame).filter(model.VideoFrame.content_id == content_id).first()
      default_video_frame_url = default_video_frame.video_frame_url
      content.video_frame = default_video_frame_url
      logging.info("video frame :: %s"%default_video_frame_url)
      urlfetch.set_default_fetch_deadline(60)
      s3_response = urlfetch.fetch(default_video_frame_url)
      if s3_response and s3_response.status_code == 200:
        image_data = s3_response.content
        content.video_frame_height = images.Image(image_data= image_data).height
        content.video_frame_width = images.Image(image_data= image_data).width
      content.video_frame_url = None
      session.merge(content)
      session.flush()
      session.commit()
    except Exception as e:
      logging.exception(e)
      support_email_for_content_processing_error(content, "Failed while saving default video frame and its resolution.")
  return


def support_email_for_content_processing_error(content, message):
  taskqueue.add(url='/tasks/support_email_for_content_processing_error',
      method='POST',
      queue_name='optimize-queue',
      params={ 
                'content_id': content.id,
                'msg' : message
      })

def create_content_status(content_ids, status):
  if content_ids and status:
    content_list = []
    for content_id in content_ids:
      content_list.append({
          "content_id": content_id,
          "status" : status,
          "added_date" : datetime.datetime.now()
      })

    if content_list:
        query = model.ContentStatus.__table__.insert().values(content_list)
        libray_data = session.execute(query)
        session.commit()

def delete_content_status(content_ids, find_status):
  if content_ids and find_status:
    delete_status = session.query(model.ContentStatus).\
                                    filter(model.ContentStatus.content_id.in_(content_ids)).\
                                    filter(model.ContentStatus.status == find_status)

    delete_status.delete(synchronize_session='fetch')
    session.commit()

def support_email_for_content_processing_error(content, message):
  taskqueue.add(url='/tasks/support_email_for_content_processing_error',
      method='POST',
      queue_name='optimize-queue',
      params={ 
                'content_id': content.id,
                'msg' : message
      })

def create_content_status(content_ids, status):
  if content_ids and status:
    content_list = []
    for content_id in content_ids:
      content_list.append({
          "content_id": content_id,
          "status" : status,
          "added_date" : datetime.datetime.now()
      })

    if content_list:
        query = model.ContentStatus.__table__.insert().values(content_list)
        libray_data = session.execute(query)
        session.commit()

def delete_content_status(content_ids, find_status):
  if content_ids and find_status:
    delete_status = session.query(model.ContentStatus).\
                                    filter(model.ContentStatus.content_id.in_(content_ids)).\
                                    filter(model.ContentStatus.status == find_status)
    delete_status.delete(synchronize_session='fetch')
    session.commit()

def get_content_type(content_format):
  content_type = None
  if content_format in constants.VIDEO_TYPE_FORMATS:
    content_type = 1
  elif content_format in constants.AUDIO_TYPE_FORMATS:
    content_type = 2
  else:
    content_type = None
  return content_type

def rescale_image(image_data, max_width=1080):

  blob_info = gtools.write_blob(image_data)
  img = images.Image(blob_key=blob_info)

  img_height = images.Image(image_data= image_data).height
  img_width = images.Image(image_data= image_data).width
  logging.info("origional image size :: " + str(img_width) + "*" + str(img_height))

  new_width = max_width if img_width > max_width else img_height
  new_height = int((img_height * new_width) / img_width)
  logging.info("new image size :: " + str(new_width) + "*" + str(new_height))
  img.resize(width=new_width, height=new_height)
  thumbnail = img.execute_transforms(output_encoding=images.JPEG)
  if blob_info:
    logging.info("deleting blob_key :: %s"%str(blob_info.key()))
    blobstore.delete(blob_info.key())
  return thumbnail
