#!/usr/bin/env python
import datetime

from google.appengine.ext import blobstore
from google.appengine.ext import ndb
from google.appengine.ext import db



class DS_User(ndb.Model):
    id_ = ndb.StringProperty(required=False)
    name = ndb.StringProperty(required=False)
    username = ndb.StringProperty(required=False)
    email = ndb.StringProperty()
    password = ndb.StringProperty(required=False)
    fb_name = ndb.StringProperty(indexed=False)
    twitter_name =  ndb.StringProperty(indexed=False)
    is_active = ndb.IntegerProperty(default=1, indexed=False)
    is_first_time = ndb.BooleanProperty(default=False)
    added_date = ndb.DateTimeProperty(auto_now_add=True)
    modified_date = ndb.DateTimeProperty(auto_now=True)
    last_login = ndb.DateTimeProperty(auto_now=True)
    twitter_id = ndb.StringProperty(indexed=False)
    fb_id = ndb.StringProperty(indexed=False)
    fb_profile_picture_url = ndb.StringProperty()
    user_type = ndb.IntegerProperty(default=0, indexed=False) 
    is_admin = ndb.BooleanProperty(default=False)

    facebook_token =  ndb.StringProperty(indexed=False)
    twitter_token = ndb.StringProperty(indexed=False)
    twitter_secret = ndb.StringProperty(indexed=False)

    image = ndb.StringProperty()
    blur_image = ndb.StringProperty()
    fb_toggle = ndb.BooleanProperty(default=True)
    twitter_toggle = ndb.BooleanProperty(default=True)
    playlist_toggle = ndb.BooleanProperty(default=True)
    s3_image_key = ndb.StringProperty()
    s3_blur_image_key = ndb.StringProperty()
    s3_compressed_image_key = ndb.StringProperty()


class DS_Album(ndb.Model):
    id_ = ndb.StringProperty(required=False)
    album_name = ndb.StringProperty(required=True)
    info = ndb.TextProperty(indexed=False)
    image = ndb.StringProperty()
    blur_image = ndb.StringProperty()
    user = ndb.StringProperty()
    added_date = ndb.DateTimeProperty(auto_now_add=True)
    modified_date = ndb.DateTimeProperty(auto_now=True)
    sort_order =  ndb.IntegerProperty(default=1, indexed=False)
    is_hidden =  ndb.IntegerProperty(default=0, indexed=False)
    expire_time = ndb.DateTimeProperty()
    s3_image_key = ndb.StringProperty()
    s3_blur_image_key = ndb.StringProperty()
    s3_compressed_image_key = ndb.StringProperty()

class DS_Genre(ndb.Model):
  id_  = ndb.StringProperty(required=False)
  name = ndb.StringProperty(required=False)

class DS_Content(ndb.Model):
  id_ = ndb.StringProperty(required=False)
  lyrics = ndb.TextProperty(indexed=False)
  title = ndb.StringProperty(indexed=False)
  heading =  ndb.StringProperty(indexed=False)
  duration =  ndb.IntegerProperty(default=0, indexed=False)
  format =  ndb.StringProperty(indexed=False)
  file_url = ndb.StringProperty()
  file_name =  ndb.StringProperty(indexed=False)
  content_type = ndb.IntegerProperty(default=2)
  buckets =  ndb.StringProperty(indexed=False)
  is_hidden = ndb.IntegerProperty(default=0, indexed=True)
  sort_order = ndb.IntegerProperty(indexed=False)

  expire_time = ndb.DateTimeProperty()
  added_date = ndb.DateTimeProperty(auto_now_add=True)
  modified_date = ndb.DateTimeProperty(auto_now_add=True)

  s3_response =  ndb.TextProperty(indexed=False)
  s3_etag =  ndb.StringProperty(indexed=False)
  s3_location = ndb.StringProperty()
  s3_Key =  ndb.StringProperty(indexed=False)
  s3_Key_before_convert =  ndb.StringProperty(indexed=False)

  s3_frame_url = ndb.StringProperty()
  s3_frame_Key =  ndb.StringProperty(indexed=False)
  video_frame =  ndb.StringProperty(indexed=False)
  video_frame_height =  ndb.IntegerProperty(default=0, indexed=False)
  video_frame_width =  ndb.IntegerProperty(default=0, indexed=False)

  genre = ndb.StructuredProperty(DS_Genre)
  user = ndb.StringProperty()
  album = ndb.StructuredProperty(DS_Album)

  current_playing_score =  ndb.FloatProperty(default=0.0, indexed=False)
  play_count= ndb.IntegerProperty(default=0, indexed=False)
  share_count= ndb.IntegerProperty(default=0, indexed=False)
  comment_count = ndb.IntegerProperty(default=0, indexed=False)
  like_count = ndb.IntegerProperty(default=0, indexed=False)
  real_time_score = ndb.FloatProperty(default=0.0, indexed=False)
  is_converted = ndb.BooleanProperty(default=False)

class DS_Party(ndb.Model):
  id_ = ndb.StringProperty(required=False)
  name = ndb.StringProperty(required=True)
  user = ndb.StringProperty()
  passcode = ndb.StringProperty()
  is_public = ndb.BooleanProperty(default=False)
  is_unlisted = ndb.BooleanProperty(default=False)
  is_passcode_enabled = ndb.BooleanProperty(default=False)
  host_queue_id = ndb.IntegerProperty(default=0)
  guest_queue_id = ndb.IntegerProperty(default=0, indexed=False)
  playlist_queue_id = ndb.IntegerProperty(default=0, indexed=False)
  post_in_feeds = ndb.BooleanProperty(default=False)
  is_deleted = ndb.BooleanProperty(default=False)
  expiration_time =  ndb.DateTimeProperty()
  is_playlist_save = ndb.BooleanProperty(default=False)
  added_date = ndb.DateTimeProperty(auto_now_add=True)


class DS_Playlist(ndb.Model):
  id_ = ndb.StringProperty(required=False)
  name = ndb.StringProperty(indexed=False)
  user =  ndb.StringProperty()
  party =  ndb.StructuredProperty(DS_Party)
  added_date = ndb.DateTimeProperty(auto_now_add=True)
  is_public = ndb.BooleanProperty(default=False)
  s3_image_key = ndb.StringProperty()
  content_count = ndb.IntegerProperty(default=0)

class DS_PlaylistContent(ndb.Model):
  id_ = ndb.StringProperty(required=False)
  content = ndb.StructuredProperty(DS_Content)
  playlist = ndb.StructuredProperty(DS_Playlist)
  playlist_key =  ndb.KeyProperty(kind='DS_Playlist')
  content_key =  ndb.KeyProperty(kind='DS_Content')
  added_date = ndb.DateTimeProperty(auto_now_add=True)

class DS_PostActivity(ndb.Model):
  id_ = ndb.StringProperty(required=False)
  post = ndb.KeyProperty(kind='DS_Post')
  user = ndb.StringProperty()
  comment = ndb.TextProperty(indexed=False)
  activity_type = ndb.StringProperty(indexed=True)
  added_date = ndb.DateTimeProperty(auto_now_add=True)
  post_user = ndb.StringProperty()
  is_viewed = ndb.BooleanProperty(default=False)

class DS_Comment(ndb.Model):
  id_ = ndb.StringProperty(required=False)
  comment = ndb.TextProperty(indexed=False)
  vote = ndb.IntegerProperty(default=0, indexed=False)
  content = ndb.StructuredProperty(DS_Content)
  user = ndb.StringProperty()
  added_date = ndb.DateTimeProperty(auto_now_add=True)
  modified_date = ndb.DateTimeProperty(auto_now_add=True)

class DS_Comment_Vote(ndb.Model):
  id_ = ndb.StringProperty(required=False)
  comment_id = ndb.KeyProperty(kind='DS_Comment', indexed=True)
  user_id = ndb.KeyProperty(kind='DS_User', indexed=True)
  vote_type = ndb.IntegerProperty(default=0, indexed=False)
  vote_date = ndb.DateTimeProperty(auto_now_add=True)
  modified_date = ndb.DateTimeProperty(auto_now_add=True)

class DS_Post(ndb.Model):
  id_ = ndb.StringProperty(required=False)
  shared_user = ndb.StringProperty()
  user = ndb.StringProperty()
  event_type = ndb.StringProperty(indexed=False)
  added_date = ndb.DateTimeProperty(auto_now_add=True)
  modified_date = ndb.DateTimeProperty(auto_now_add=True)
  comment = ndb.TextProperty(indexed=False)
  content = ndb.StructuredProperty(DS_Content)
  party = ndb.StructuredProperty(DS_Party)
  playlist = ndb.StructuredProperty(DS_Playlist)
  event_for = ndb.StringProperty(indexed=False)
  comments = ndb.StructuredProperty(DS_PostActivity, repeated=True)
  likes = ndb.StructuredProperty(DS_PostActivity, repeated=True)
  total_comment = ndb.IntegerProperty(default=0, indexed=False)
  total_like = ndb.IntegerProperty(default=0, indexed=False)
  content_comment = ndb.StructuredProperty(DS_Comment)

class DS_Realtime_Content_Activity(ndb.Model):
  user = ndb.StringProperty()
  content = ndb.StructuredProperty(DS_Content)
  content_id = ndb.StringProperty()
  genre_id = ndb.StringProperty()
  content_type = ndb.IntegerProperty()
  is_hidden = ndb.IntegerProperty(default=0, indexed=True)
  activity = ndb.StringProperty()
  added_date = ndb.DateTimeProperty(auto_now_add=True)
  modified_date = ndb.DateTimeProperty()
  expiration_time =  ndb.DateTimeProperty()

class DS_Past_Content_Activity(ndb.Model):
  user = ndb.StringProperty()
  content = ndb.StructuredProperty(DS_Content)
  content_id = ndb.StringProperty()
  genre_id = ndb.StringProperty()
  content_type = ndb.IntegerProperty()
  is_hidden = ndb.IntegerProperty(default=0, indexed=True)
  activity = ndb.StringProperty()
  added_date = ndb.DateTimeProperty(auto_now_add=True)
  modified_date = ndb.DateTimeProperty()
  expiration_time =  ndb.DateTimeProperty()
